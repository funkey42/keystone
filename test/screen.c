#include "core.h"
#include "i41.h"
#include "rre_8x12.h"

/*
void *menu[] = {
	"Turn off screen\0" "\x02\x01"
	"Macros...\0"       "\x01\x00\0",
	NULL,

	"Add macro\0"       "\x03\x01"
	"Edit macro\0"      "\x04\x01"
	"Remove macro\0"    "\x05\x01\0",
	NULL, // Add macro
	NULL, // Edit macro
	NULL  // Remove macro
};
*/

const static RRE_Font *font = &rre_8x12;
const static u16 font_height = 12;
const static u16 font_scale = 1;
t_cursor cursor = {0, 0};

void setup(void) {
	const u16 scroll_margin = SCREEN_HEIGHT % (font_height * font_scale);
	DDRC |= (1<<DDC6); // Set screen backlight control as output
	PORTC |= (1<<PC6); // Set screen backlight at full brightness
	DDRB |= (1<<DDB0); // Enable driving pro micro Tx Led for debug
	i41_power_on();
	i41_dispon();
	i41_fill_screen(0x0000);
	i41_scroll_area(scroll_margin, SCREEN_HEIGHT - scroll_margin, 0);
}

u16 i = 0;

void loop(void) {
	cursor = i41_draw_string(cursor, font, 1, 0x07e0, 0x001f,
			"Pas normal c'est un peu chiant.\n"
			">Tu fais un Lorem Ipsum?\n"
			";.\\'|'''\n");
	_delay_ms(10);
	cursor = i41_draw_string(cursor, font, 1, 0x07e0, 0xf800,
			"Pas normal c'est un peu chiant.\n"
			">Tu fais un Lorem Ipsum?\n"
			";.\\'|'''\n");
	i41_fill_rect(10, 10, 30, 30, 0x0000);
	_delay_ms(10);
}

int main(void) {
	setup();
	while (1) {
		loop();
	}
}
