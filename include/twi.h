#pragma once

#include <inttypes.h>

#ifndef TWI_FREQ
#define TWI_FREQ 100000L
#endif

#ifndef TWI_BUFFER_LENGTH
#define TWI_BUFFER_LENGTH 128
#endif

#define TWI_READY	0
#define TWI_MRX		1
#define TWI_MTX		2
#define TWI_SRX		3
#define TWI_STX		4

void	twi_init(void);
void	twi_disable(void);
void	twi_set_address(uint8_t);
void	twi_set_frequency(uint32_t);
uint8_t	twi_read_from(uint8_t, uint8_t*, uint8_t, uint8_t);
uint8_t	twi_write_to(uint8_t, uint8_t*, uint8_t, uint8_t, uint8_t);
uint8_t	twi_transmit(const uint8_t*, uint8_t);
void	twi_attach_slave_rx_event( void (*)(uint8_t*, int) );
void	twi_attach_slave_tx_event( void (*)(void) );
void	twi_reply(uint8_t);
void	twi_stop(void);
void	twi_release_bus(void);
void	twi_set_timeout_in_micros(uint32_t, bool);
void	twi_handle_timeout(bool);
bool	twi_manage_timeout_flag(bool);
