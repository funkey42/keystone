#pragma once
// Arduino's definition of time in us
unsigned long micros();
// Arduino's definition of time in ms
unsigned long millis();
