#pragma once
#include "core.h"

// ILI9341 interface for ATmega32u4

#if defined(SCREEN_WIDTH) && defined(SCREEN_HEIGHT)
#else
#warning SCREEN_WIDTH and SCREEN_HEIGHT undefined. Setting to 240 and 320.
#define SCREEN_WIDTH 240
#define SCREEN_HEIGHT 320
#endif

#if defined(PORT_DC) && defined(BIT_DC)
#else
#warning [PORT/BIT]_DC is undefined. Setting to PORTB and PINB7.
#define PORT_DC    PORTB
#define BIT_DC     PINB7
#endif

#if defined(PORT_RST) && defined(BIT_RST)
#else
#warning [PORT/BIT]_RST is undefined. Setting to PORTB and PINB6.
#define PORT_RST        PORTB
#define BIT_RST         PINB6
#endif

#if defined(PORT_CS) && defined(BIT_CS)
#else
#warning [PORT/BIT]_CS is undefined. Setting to PORTB and PINB5.
#define PORT_CS         PORTB
#define BIT_CS          PINB5
#endif

#if defined(PORT_BL) && defined(BIT_BL)
#else
#warning [PORT/BIT]_BL is undefined. Setting to PORTB and PINB6.
#define PORT_BL  PORTB
#define BIT_BL   PINB6
#endif

/// Smoothstep between 2 values on an u8
/// @param _x value to smoothstep
/// @param _min minimum value of the sigmoid
/// @param _max maximum value of the sigmoid
#define SMOOTHSTEPu8(_x, _min, _max) \
	((_x) < (_min) ? 0 : \
		((_x) > (_max) ? 255 : \
			((u16)(_x) - (_min)) * 255 / ((_max) - (_min))))

/// RGB to 16 bit int
/// @param r red channel (will be clipped)
/// @param g green channel (will be clipped)
/// @param b blue channel (will be clipped)
#define RGBto565(r, g, b) (((r) & 0xf8) << 8 \
						| ((g) & 0xfc) << 3 | ((b) & 0xf8) >> 3)

// https://github.com/cbm80amiga/RREFont
/// RRE type used to know the type of font used
/// It enables faster calculation
typedef enum RRE_Type {
	RRE_16B =        0, //< 16x16, rects (X4Y4W4H4)
	RRE_V16B =       1, //< 64x32, lines (X6Y5W0H5)
	RRE_H16B =       2, //< 32x64, lines (X5Y6W5H0)
	RRE_24B =        3, //< 64x64, rects (X6Y6W6H6)
	RRE_V24B =       4, //< 256x256, vertical   lines (X8Y8W0H8)
	RRE_H24B =       5, //< 256x256, horizontal lines (X8Y8W8H0)
	RRE_32B =        6, //< 256x256, rects (X8Y8W8H8)
	RRE_P8B =        7, //< 16x16,   pixels (X4Y4W0H0)
	RRE_P16B =       8, //< 256x256, pixels (X8Y8W0H0)
	RRE_NO_SORT = 0x80, //< old fonts not optimized for fast width calculation
} RRE_Type;

/// Font data structure that consists of rectangle
/// instead of regular bitmap.
struct RRE_Font {
	u8 type;			//< Font type width of data
	u8 wd;				//< Width of the font
	u8 ht;				//< Height of the font
	u8 firstCh;			//< First char index
	u8 lastCh;			//< Last char index
	const u8 *rects;	//< Actual rectangles of the font
	const u16 *offs;	//< Rectangles offsets
};
typedef struct RRE_Font RRE_Font;

typedef enum i41_command_code {
	// No command code
	I41_NOP =     0x00,
	/// Exits sleep
	/// Takes 120ms. Also known as SPLOUT in spec.
	I41_SLPOUT =  0x11,
	/// Enters sleep
	/// Takes 120ms. Also known as SPLIN in spec.
	I41_SLPIN =   0x10,
	/// Turns the display off
	I41_DISPOFF = 0x28,
	/// Turns the display on
	I41_DISPON =  0x29, 
	/// Raw memory write
	I41_RAMWR =   0x2c,
} i41_cmd_code;

struct s_cursor {
	u16 x;
	u16 y;
};
typedef struct s_cursor t_cursor;

struct s_slider_data {
	u8 *value;
	u8 max;
	u16 *color;
	void (*callback)(u8);
};
typedef struct s_slider_data t_slider_data;

/*
** Global variables
*/

extern u8 g_backlight;

// Low level screen operations

/// Writes a command code on the SPI bus
void i41_write_command(u8 command);
/// Writes data on the SPI bus
void i41_write_data(u8 data);
/// Reads a byte of data of the SPI bus
u8 i41_read_data();
/// Power sequence
void i41_power_on();
/// Execute the command sleep enter
void i41_slpin();
/// Execute the command sleep exit
void i41_slpout();
/// Turns off the display
void i41_dispoff();
/// Define the area that gets scrolled (VSCRDEF)
void i41_scroll_area(u16 tfa, u16 vsa, u16 bfa);
/// Scroll the display (VSCRSADD)
void i41_scroll(u16 vsp);
/// Turns on the display
void i41_dispon();
/// Sets the screen's viewport rectangle
/// @param x1 start on x axis
/// @param y1 start on y axis
/// @param x2 end on x axis
/// @param y2 end on y axis
void i41_viewport(u16 x1, u16 y1, u16 x2, u16 y2);
/// Fills the screen with a 16 bit rgb color
/// @param rgb the 16 bit rgb color
void i41_fill_screen(u16 rgb);
/// Fills the screen with a gradient
void i41_fill_screen_with_gradiant();
/// Fills the screen with a rectangle of color at a given position
/// @param x1 start on x axis
/// @param y1 start on y axis
/// @param x2 end on x axis
/// @param y2 end on y axis
/// @param color the 16 bit rgb color
void i41_fill_rect(u16 x1, u16 y1, u16 x2, u16 y2, u16 color);
void i41_draw_rect(u16 x1, u16 y1, u16 x2, u16 y2, u16 color, i16 thickness);
void i41_set_backlight(u8 backlight);

// Text rendering

u16 i41_rre_char_width_16b(const RRE_Font *font, u8 c);
/// Draws a char from a font on the screen at a position
/// @param font an RRE font (assumes the font is in EEPROM)
/// @param fg 16 bit color for the foreground
/// @param bg 16 bit color for the background
/// @param x x position on the screen
/// @param y y position on the screen
/// @param c the character to print
u16 i41_draw_character(const RRE_Font *font, u16 scale,
		u16 fg, u16 bg, u16 x, u16 y, u8 c);
/// Draws a char from a font on the screen at a position.
/// This is faster than the `i41_draw_character` method but uses more space
/// for preprocessing purposes.
/// @param font an RRE font (assumes the font is in EEPROM)
/// @param fg 16 bit color for the foreground
/// @param bg 16 bit color for the background
/// @param x x position on the screen
/// @param y y position on the screen
/// @param c the character to print
u16 i41_blit_character(const RRE_Font *font,
		u16 fg, u16 bg, u16 x, u16 y, u8 c);
t_cursor i41_draw_string(t_cursor cursor, const RRE_Font *font, u16 scale,
                         u8 monospaced, u16 fg, u16 bg, const char *str);
t_cursor i41_putnumber(t_cursor cursor, const RRE_Font * font, u16 scale,
                         u8 monospaced, u16 fg, u16 bg, i16 number);

void i41_init_menu(void *new_menu, const RRE_Font *new_font);
void i41_process_menu();
void i41_menu_slider_hook(t_slider_data *slider_data);
void i41_menu_sleep(void *data);
void i41_menu_terminal(void *dialog_data);

void i41_init();
