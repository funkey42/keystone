#pragma once
#include "core.h"

/// Starts the serial interface (USB CBC)
void serial_start();
void serial_wait();

void serial_print_str(char *str);
void serial_print_ul(unsigned long m);
void serial_print_u8(u8 u);
void serial_print_i8(i8 u);
void serial_print_i16(i16 u);
void serial_print_u16(u16 u);
void serial_print_f(float u);

int serial_available();
int serial_read();
