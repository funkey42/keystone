#pragma once
#include <avr/io.h>
#include "core.h"

// Macro definition verification
#if defined(LED_port_data) && defined(LED_pin_data)
#else
#warning "LED_port_data and LED_pin_data not defined. Setting to PORTD and PIND3."
#define LED_port_data PORTD
#define LED_pin_data PIND3
#endif
#if defined(LED_port_clock) && defined(LED_pin_clock)
#else
#warning "LED_port_data and LED_pin_data not defined. Setting to PORTD and PIND2."
#define LED_port_clock PORTD
#define LED_pin_clock PIND2
#endif

/// Utils made to map a value from a range to an other
/// @param value the value to map
/// @param low1 original range lower boundary
/// @param high1 original range higher boundary
/// @param low2 new range lower boundary
/// @param high2 new range higher boundary
#define map(value, low1, high1, low2, high2) low2 + (value - low1) * (high2 - low2) / (high1 - low1)

/// Default rgb color struct
typedef struct rgb_color {
	union {
		struct {
			u8 red; //< the red color channel
			u8 green; //< the green color channel
			u8 blue; //< the blue color channel
		};
		struct {
			u8 r;
			u8 g;
			u8 b;
		};
	};
} rgb_color;

/// An alternate color struct easier to work with
typedef struct hsv_color {
	union {
		struct {
			u16 hue; //< the color hue
			u8 saturation; //< the color saturation
			u8 value; //< the color luminance, can be used as brightness
		};
		struct {
			u16 h;
			u8 s;
			u8 v;
		};
	};
} hsv_color;

/// Sends a pulse with one bit of data on the LEDs ports
/// @param bit the bit to send (clips it to one bit)
#define led_send_bit(bit) do { \
	LED_port_data &= ~(1 << LED_pin_data); \
	LED_port_data |= ((bit) & 1) << LED_pin_data; \
	LED_port_clock |= 1 << LED_pin_clock; \
	LED_port_clock &= ~(1 << LED_pin_clock); \
} while (0)

// Utils

/// Converts hsv format to standard `rgb_color` one.
/// @param h the hue, from 0 to 360.
/// @param s the saturation, from 0 to 255.
/// @param v the value, from 0 to 255.
rgb_color hsv_to_rgb(uint16_t h, uint8_t s, uint8_t v);

/// Mixes 2 colors with a bias
/// @param a an RGB color
/// @param b an RGB color
/// @param bias the bias goes from 0 to 1
rgb_color rgb_mix(rgb_color a, rgb_color b, float bias);

// Std API

/// Inits the ports for the LEDs
void led_init();

/// Bit bang byte sending
/// @param b the byte to send to the LEDs
void led_send_byte(uint8_t b);

/// Sends the start of the frame to the LEDs
/// This boils down to 32 bits of 0.
void led_start_frame();

/// Sends the end of the frame to the LEDs
/// You need the length because of an error in the spec.
/// Normally you would need to send 32 bits of 1 but doesn't work
/// when the number of LEDs > 60.
/// To work around that you send fewer bits the more LEDs you have
/// @param leds the number of leds
void led_end_frame(uint16_t leds);

/// Writes a full frame to the LEDs
/// @param colors the color table where the first index is the first LED
/// @param count the size of the color table
/// @param brightness the global brightness for all the LEDs
void led_write(rgb_color *colors, uint16_t count, uint8_t brightness);