#pragma once
#include "core.h"
#include <avr/eeprom.h>

/// Saves a bit of data to the eeprom knowing it's size
/// Note: this asumes that the size of both pointer is the same
/// @param data a pointer to the data to save
/// @param eeprom pointer to the eeprom mapping
/// @param size size of the data to save
void eeprom_save(void *data, void *eeprom, u8 size);

/// Load a bit of data from the eeprom knowing it's size
/// Note: this asumes that the size of both pointer is the same
/// @param data a pointer to the data to load to
/// @param eeprom pointer to the eeprom mapping
/// @param size size of the data to load
void eeprom_load(void *data, void *eeprom, u8 size);
