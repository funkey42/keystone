#pragma once
#include "core.h"

#if defined(ENC_port_plus) && defined(ENC_pin_plus)
#else
#warning "ENC_port_plus and ENC_pin_plus not defined. Setting to PINB and PINB4."
#define ENC_port_plus PINB
#define ENC_pin_plus PINB4
#endif
#if defined(ENC_port_minus) && defined(ENC_pin_minus)
#else
#warning "ENC_port_minus and ENC_pin_minus not defined. Setting to PINB and PINB5."
#define ENC_port_minus PINB
#define ENC_pin_minus PINB5
#endif
#if defined(ENC_port_push) && defined(ENC_pin_push)
#else
#warning "ENC_port_push and ENC_pin_push not defined. Setting to PINB and PINB6."
#define ENC_port_push PINB
#define ENC_pin_push PINB6
#endif

/// Inits the necessary interrupts to enable the encoder behavior
void	encoder_init();

/// Update the internal states of the encoder
/// it's to be called in the interrupt for the encoder pins
void	encoder_update();

/// Gets the encoder + or - steps from the last time this was called
i16		encoder_rotation();

/// Gets the encoder push button state
bool	encoder_pushed();

/// Function to enable interrupts on the encoder.
/// This must be defined by the user.
void	encoder_interrupt_enable();