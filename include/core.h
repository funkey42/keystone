#pragma once
// Includes the config so everything is set to compile
#include "config.h"

// Some default includes
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/io.h>
#include <util/delay.h>

// Types shorthand

/// Unsigned int of size 8 bits
typedef uint8_t		u8;
/// Unsigned int of size 16 bits
typedef uint16_t	u16;
/// Unsigned int of size 32 bits
typedef uint32_t	u32;
/// Signed int of size 8 bits
typedef int8_t		i8;
/// Signed int of size 16 bits
typedef int16_t		i16;
/// Signed int of size 32 bits
typedef int32_t		i32;

// String concatenation with preprocessor

/// Concatenate 2 words
/// @param x word to concatenate
/// @param y word to be concatenated
#define CONCAT(x, y) x##y
/// Concatenate 2 expanded words
/// @param x expanded word to concatenate
/// @param y expanded word to concatenate
#define CONCAT_EXPANDED(x, y) CONCAT(x, y)
/// Convert from word to string
/// @param x word to stringify
#define STRINGIFY(x) #x
/// Convert from expanded word to string
/// @param x expanded word to stringify
#define STRINGIFY_EXPANDED(x) STRINGIFY(x)

// Bit manipulation

/// Least significant byte on a 16 bit integer
/// @param _x byte to mask
#define LSB(_x) ((_x) & 0xFF)
/// Most significant byte on a 16 bit integer
/// @param _x byte to shift
#define MSB(_x) ((_x) >> 8)

// Simple math functions

/// Caps a value `_x` under or equal `_y`
/// @param _x value to cap
/// @param _y maximum of the value
#define CAP(_x, _y) ((_x) > (_y) ? (_y) : (_x))
/// Absolute value
/// @param _x value of which to return the absolute
#define ABS(_x) ((_x) < 0 ? -(_x) : (_x))
/// Clamps a value `_x' between `_max` and `_min`.
/// Returns `_min` if `_x` < `_min`.
/// Returns `_max` if `_x` > `_max`.
/// Returns `_x` otherwise.
#define CLAMP(_x, _min, _max) \
	((_x) < (_min) ? _min : ((_x) > (_max) ? (_max) : (_x)));

#define MIN(a, b) ((a) < (b) ? (a) : (b))

// Macros to access ports and pins from the macro name

/// Address of the port data-in of port X
/// Use use as `PIN(PORTB)`
#define PIN(x) (*(&x - 2))
/// Address Of Input Register Of Port x
/// Use use as `DDR(PORTB)`
#define DDR(x) (*(&x - 1))
/// Address of the port data-out of port X
/// Nothing is done to it
#define PORT(x) (x)
