#pragma once
#include "keyboard.h"

#if defined(ANIM_key_width) && defined(ANIM_key_height)
#else
#warning "ANIM_key_width and ANIM_key_height not defined. Setting to 6 and 4"
#define ANIM_key_width 6
#define ANIM_key_height 4
#endif
#if defined(ANIM_back_width) && defined(ANIM_back_height)
#else
#warning "ANIM_back_width and ANIM_back_height not defined. Setting to 3 and 2"
#define ANIM_back_width 3
#define ANIM_back_height 2
#endif

#define ANIM_led_count ((ANIM_key_width) * (ANIM_key_height) \
					+ (ANIM_back_width) * (ANIM_back_height))

/// Magic number to determine if the config already has been initialiazed
#define ANIM_magic	(0x41)
/// The config EEPROM offset
#define ANIM_config	((void*)0x00)

/// LED animation types
typedef enum animation {
	/// No animation
	ANIM_none = 0,
	/// A rainbow wave of color.
	ANIM_color_wave,
	/// A fade in and out of a color.
	/// Primary key backlight
	/// Accent underglow
	ANIM_breathing,
	/// Keys pressed are lit up by with color
	/// and then fade to an other.
	/// Primary key backlight
	/// Accent key fade
	ANIM_reactive,
	/// Rainbow reactive color
	ANIM_color_reactive,
	/// Array boundary
	ANIM_max,
} animation;

/// Animation config to store and load from the EEPROM
typedef struct __attribute__((packed)) animation_config {
	/// Is the config initialiazed
	u8			magic;

	/// Whether or not display the back animation
	bool		display_back : 1;
	/// Whether or not display the front animation
	bool		display_front : 1;
	/// Animation to display
	animation	anim;

	/// Animation duration reference in ms
	float		duration;
	/// The speed multiplier of the animations
	float		speed;
	/// The global color for some of the animations using custom colors.
	hsv_color	primary;
	/// The accent color for some of the animations using custom colors.
	hsv_color	accent;
} animation_config;

/// Default prototype for LED animations
/// @param time the time from 0 to 1
/// @param primary the HSV color to be used as primary
/// @param accent the HSV color to be used as accent
/// @param report the matrix report of the key pressed
typedef void (*led_animation)(
	float buffer[ANIM_key_width][ANIM_key_height],
	float seed,
	float time,
	hsv_color primary,
	hsv_color accent,
	kb_keycode report[ANIM_key_width][ANIM_key_height]
);

/// Animation wrapper to use as a hub to display an animation on a LED matrix
/// @param anim the animation id to use
/// @param report the matrix report of the key pressed
void animate_leds(matrix_report *report);

/// Loading the animation config from eeprom
void animate_init();

/// Set the animation variant
/// @param anim the animation to set to (can overflow)
void animate_set_anim(animation anim);

/// Set the speed multiplier of the animation
/// @param speed the multiplier
void animate_set_speed(float speed);

/// Set the duration duration of interactive animations
/// @param duration the duration in ms
void animate_set_duration(float duration);

/// Set the primary color
/// @param color the color
void animate_set_primary(hsv_color color);

/// Set the accent color
/// @param color the color
void animate_set_accent(hsv_color color);

/// Gets the config for the animation
/// @return the config
animation_config animate_get_config();

/// Gets the config for the animation
/// @return the config
animation animate_get_anim();
