include $(dir $(lastword $(MAKEFILE_LIST)))/strings.mk

define cmd
	@printf $(SUB_MAKE_START)"\n"
	@make -w -C $(1) $(2) $(3)
	@printf $(SUB_MAKE_DONE)"\n"
endef
