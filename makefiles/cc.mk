include $(dir $(lastword $(MAKEFILE_LIST)))/strings.mk
# Building rules for c/cpp/assembly

$(PATH_OBJ)/%.o: $(PATH_SRC)/%.c | $$(@D)/.
	@printf $(COMPILING)"\n"
	@$(CC) -o $@ -c $(CFLAGS) $<

$(PATH_OBJ)/%.o: $(PATH_SRC)/%.cpp | $$(@D)/.
	@printf $(COMPILING)"\n"
	@$(CCPP) -o $@ -c $(CPPFLAGS) $<

$(PATH_OBJ)/%.o: $(PATH_SRC)/%.S | $$(@D)/.
	@printf $(COMPILING)"\n"
	@$(CC) -x assembler-with-cpp $(CPPFLAGS) -o $@ -c $<

# Cleaning rule

clean:
	@printf $(CLEANING)"\n"
	@rm -rf $(TO_CLEAN)
	@$(TO_CLEAN_EXEC)
