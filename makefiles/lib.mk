include $(dir $(lastword $(MAKEFILE_LIST)))/strings.mk
# Creates an archive for c like libraries

build: $(PATH_TARGET)/$(TARGET).a

$(PATH_TARGET)/$(TARGET).a: $(OBJS) | $$(@D)/.
	@printf $(LIB_BUILD)"\n"
	@$(AR) rc $(PATH_TARGET)/$(TARGET).a $(OBJS)
	@printf $(LIB_INDEX)"\n"
	@$(RANLIB) $(PATH_TARGET)/$(TARGET).a
