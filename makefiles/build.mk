include $(dir $(lastword $(MAKEFILE_LIST)))/strings.mk
# Directory creation rules

.PRECIOUS: $(PATH_BUILD)/. $(PATH_BUILD)%/.

$(PATH_BUILD)/.:
	@mkdir -p $@

$(PATH_BUILD)%/.:
	@mkdir -p $@

.SECONDEXPANSION:
