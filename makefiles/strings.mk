NRM := "\e[0m"

INFO := "\e[37m["$(NRM)" \e[1;33mi"$(NRM)" \e[37m]"$(NRM)
WARN := "\e[37m["$(NRM)" \e[1;31m-"$(NRM)" \e[37m]"$(NRM)
FAIL := ""
VALID := "\e[37m["$(NRM)" \e[1;32m+"$(NRM)" \e[37m]"$(NRM)

# Strings to use as output for the user
# Compilation for an object
COMPILING = $(VALID)" \e[1;3;32mCompiling \e[0;33m"$(subst /,"\e[2m/\e[22m",$(subst $(PWD),.,$(dir $(abspath $<))))"\e[1m"$(basename $(notdir $<))"\e[0;3;33m"$(suffix $<)$(NRM)
# Cleaning the build directory
CLEANING = $(WARN)" \e[1;3;31mCleaning \e[0;33mbuild cache for $(TARGET) \e[2;3m(\e[1m"$(PATH_BUILD)"\e[21m)"$(NRM)
# Building a library from objects
LIB_BUILD = $(INFO)" \e[1;3;35mCreating \e[0;33mlibrary \e[32m"$(subst /,"\e[2m/\e[22m",$(subst $(PWD),.,$(dir $(abspath $@))))"\e[1m"$(basename $(notdir $@))"\e[0;3;32m"$(suffix $@)$(NRM)
LIB_INDEX = $(INFO)" \e[1;3;35mCreating \e[0;33mindex for \e[32m"$(subst /,"\e[2m/\e[22m",$(subst $(PWD),.,$(dir $(abspath $@))))"\e[1m"$(basename $(notdir $@))"\e[0;3;32m"$(suffix $@)$(NRM)
# Make sub system
SUB_MAKE_START = $(INFO)" \e[1;3;35mMaking \e[0;2;1;35m"$(notdir $(1))" \e[33;3;1m"$(2)$(NRM)
SUB_MAKE_DONE = $(VALID)" \e[1;3;35mMaking \e[0;2;1;35m"$(notdir $(1))" \e[33;3;1m"$(2)"\e[0;1;2;33m is done."$(NRM)
# Binary compilation
COMPILING_FIRM = $(VALID)" \e[1;3;35mCompiling \e[1;2;3;35melf firmware \e[0;32m"$(subst /,"\e[2m/\e[22m",$(subst $(PWD),.,$(dir $(abspath $@))))"\e[1m"$(basename $(notdir $@))"\e[21;22;3m"$(suffix $@)$(NRM)
MAKING_HEX = $(VALID)" \e[1;3;35mMaking \e[1;2;3;35mhex firmware \e[0;32m"$(subst /,"\e[2m/\e[22m",$(subst $(PWD),.,$(dir $(abspath $@))))"\e[1m"$(basename $(notdir $@))"\e[21;22;3m"$(suffix $@)$(NRM)
# Making directory
MKDIR = $(VALID)" \e[1;2;3;32mMaking \e[22;33m"$(subst /,"\e[2m/\e[22m",$(subst $(PWD),.,$(dir $(abspath $@))))$(NRM)
# Flashing of the binary
OPENCLOSE = $(INFO)" \e[33mOpen and closing port with \e[1;2;3m1200bps"$(NRM)
FLASHING_WAIT = $(INFO)" \e[33mWaiting for port to open"
FLASHING = $(NRM)"\n"$(INFO)" \e[1;3;35mFlashing \e[1;32m"$(TARGET)"\e[21;22;3m.hex"$(NRM)
