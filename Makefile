include ./makefiles/submake.mk
MAKEFLAGS += --no-print-directory
# Command list of commands possible to invoke
CMD_LIST = \
	flash \
	build \
	clean \
# Target list of targets possible to invoke
TARGET_LIST = \
	master \
	slave \

header:
	@printf "\n"
	@while read -r line; do printf "$$line\n"; done < makefiles/keystone.ascii
	@printf "\e[0m\n"

ifneq ($(filter $(CMD_LIST),$(firstword $(MAKECMDGOALS))),)
ifneq ($(filter $(TARGET_LIST),$(word 2, $(MAKECMDGOALS))),)
cmd_target := $(word 2, $(MAKECMDGOALS))
# Use the rest as arguments for running one of the commands
cmd_args := $(wordlist 3,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
# ... and turn them into do-nothing targets
$(eval $(cmd_args):;@:)
$(eval $(cmd_target):;@:)
# ... and generate target commands for to be run
$(foreach rule,$(CMD_LIST) all, $(eval $(rule): header;$(call cmd,src/$(cmd_target),$(rule),$(cmd_args))))
# Or generate other rules for when there isn't the required arguments
else
$(foreach rule, $(CMD_LIST) all, $(eval $(rule): ;-@echo "Usage: make [$(strip $(CMD_LIST))] [$(strip $(TARGET_LIST))] -- args"))
endif
else
$(foreach rule, $(CMD_LIST) all, $(eval $(rule): ;-@echo "Usage: make [$(strip $(CMD_LIST))] [$(strip $(TARGET_LIST))] -- args"))
endif
.PHONY: header
