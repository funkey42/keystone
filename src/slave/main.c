#include "core.h"

#include "sk9822.h"
#include "keyboard.h"
#include "animations.h"
#include "twi.h"
#include "serial.h"
#include <util/delay.h>

const port_scanner	kb_cols[KB_matrix_width/2] = {
	[5] = (port_scanner){.port = &PINB, .pin = PINB0, .ddr = &DDRB},
	[4] = (port_scanner){.port = &PINB, .pin = PINB1, .ddr = &DDRB},
	[3] = (port_scanner){.port = &PINB, .pin = PINB2, .ddr = &DDRB},
	[2] = (port_scanner){.port = &PINB, .pin = PINB3, .ddr = &DDRB},
	[1] = (port_scanner){.port = &PINB, .pin = PINB4, .ddr = &DDRB},
	[0] = (port_scanner){.port = &PINB, .pin = PINB5, .ddr = &DDRB},
};
const port_scanner	kb_rows[KB_matrix_height] = {
	[3] = (port_scanner){.port = &PORTC, .pin = PORTC0, .ddr = &DDRC},
	[2] = (port_scanner){.port = &PORTC, .pin = PORTC1, .ddr = &DDRC},
	[1] = (port_scanner){.port = &PORTC, .pin = PORTC2, .ddr = &DDRC},
	[0] = (port_scanner){.port = &PORTC, .pin = PORTC3, .ddr = &DDRC},
};

// Led colors
static rgb_color colors[ANIM_led_count] = {};
// Global scan
static kb_scan scan = (kb_scan){};

void rx_event(uint8_t* data, int length)
{
	// Recieves the led state
	if (length - 1 <= sizeof(colors)) {
		// Copies the values of data inside of the color buffer
		for (u8 i = 0; i < length; ++i) {
			((u8*)colors)[i] = data[i];
		}
	}
	/* for (u8 i = 0; i < ANIM_led_count; ++i) { */
	/* 	colors[i] = (rgb_color){.r = 255}; */
	/* } */
}

void tx_event()
{
	twi_transmit((const uint8_t*)scan.bytes, sizeof(scan.bytes));
}

int main() {
	// Disable all interrupts while we setup
	cli();
	// Init the keyboard matrix to be able to scan and send to master
	kb_init_matrix();
	// Init the leds
	led_init();
	// Init the twi ports to talk to the master
	twi_init();
	twi_set_address(0xb);
	twi_attach_slave_rx_event(&rx_event);
	twi_attach_slave_tx_event(&tx_event);
	// Enable interrupts since we are ready
	sei();
	while (true) {
		// Gets the report
		scan = kb_scan_matrix();
		/* for (u8 i = 0; i < ANIM_led_count; ++i) { */
		/* 	colors[i] = (rgb_color){.r = 255}; */
		/* } */
		// Always updates the leds
		led_write(colors, ANIM_led_count, 10);
		_delay_ms(10);
	}
}
