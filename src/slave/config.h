#pragma once

// Various pins for the LEDs
#define LED_ddr_data			DDRD
#define LED_port_data			PORTD
#define LED_pin_data			PIND0
#define LED_ddr_clock			DDRD
#define LED_port_clock			PORTD
#define LED_pin_clock			PIND4

// Led config
#define ANIM_key_width			6
#define ANIM_key_height			4
#define ANIM_back_width			3
#define ANIM_back_height		2

// Keyboard dimentions without the split
#define KB_matrix_height		4
#define KB_matrix_width			12

// TWI slave config
#define TWI_matrix_address		0x8
#define TWI_freq				1000000ul
#define TWI_sda_port			PORTC
#define TWI_sda_pin				PORTC4
#define TWI_scl_port			PORTC
#define TWI_scl_pin				PORTC5
