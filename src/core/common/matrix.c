#include "core.h"
#include "keyboard.h"
#include "serial.h"

void
kb_set_scan_bit(kb_scan *scan, u8 x, u8 y, u8 value)
{
	const u8 byte = (x + y * (KB_matrix_width / 2)) / 8;
	const u8 mask = ((x + y * (KB_matrix_width / 2)) % 8);
	scan->bytes[byte] |= (value > 0) << mask;
}

u8
kb_get_scan_bit(kb_scan *scan, u8 x, u8 y)
{
	const u8 byte = (x + y * (KB_matrix_width / 2)) / 8;
	const u8 mask = ((x + y * (KB_matrix_width / 2)) % 8);
	return (scan->bytes[byte] & (1 << mask)) > 0;
}

void
kb_init_matrix()
{
	// Set columns as outputs
	for (u8 i = 0; i < KB_matrix_width / 2; ++i) {
		*kb_cols[i].ddr &= ~(1 << kb_cols[i].pin);
		// TODO disable interanl pull ups
	}
	// Set rows as inputs
	for (u8 i = 0; i < KB_matrix_height; ++i) {
		*kb_rows[i].ddr |= (1 << kb_rows[i].pin);
	}
}

kb_scan
kb_scan_matrix()
{
	kb_scan scan = (kb_scan){};

	for (u8 i = 0; i < KB_matrix_height; ++i) {
		// Set col pin high
		*kb_rows[i].port |= 1 << kb_rows[i].pin;
		// serial_print_u8(i);
		// serial_print_str(" > ");
		for (u8 j = 0; j < KB_matrix_width / 2; ++j) {
			// Wait for the bouce to pass
			_delay_us(20);
			// Scan row pin and put in the report
			kb_set_scan_bit(&scan, j, i, *kb_cols[j].port & (1 << kb_cols[j].pin));
			// serial_print_u8(kb_get_scan_bit(&scan, j, i));
			// serial_print_u8(*kb_cols[j].port & (1 << kb_cols[j].pin));
		}
		// serial_print_str("\n");
		// Set col pin low
		*kb_rows[i].port &= ~(1 << kb_rows[i].pin);
	}
	return scan;
}
