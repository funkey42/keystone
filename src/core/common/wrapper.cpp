#include <Arduino.h>

extern "C" {
	void serial_start() {
		TXLED0;
		Serial.begin(9600);
	}
	void serial_wait() {
		while (Serial.available() <= 0);
	}
	void serial_print_str(char *str) {
		Serial.print(str);
	}
	void serial_print_ul(unsigned long n) {
		Serial.print(n);
	}
	void serial_print_u8(u8 u) {
		Serial.print(u);
	}
	void serial_print_u16(uint16_t u) {
		Serial.print(u);
	}
	void serial_print_i8(int8_t u) {
		Serial.print(u);
	}
	void serial_print_i16(int16_t u) {
		Serial.print(u);
	}
	void serial_print_f(float u) {
		Serial.print(u);
	}
	int	serial_available() {
		return Serial.available();
	}
	int	serial_read() {
		return Serial.read();
	}
	unsigned long time(void) {
		return millis();
	}
}
