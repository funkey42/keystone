#include "core.h"
#include "sk9822.h"

/// Converts hsv format to standart `rgb_color` one.
/// @param h the hue, from 0 to 360.
/// @param s the saturation, from 0 to 255.
/// @param v the value, from 0 to 255.
rgb_color
hsv_to_rgb(
	uint16_t h,
	uint8_t s,
	uint8_t v
) {
	uint8_t f = (h % 60) * 255 / 60;
	uint8_t p = (255 - s) * (uint16_t)v / 255;
	uint8_t q = (255 - f * (uint16_t)s / 255) * (uint16_t)v / 255;
	uint8_t t = (255 - (255 - f) * (uint16_t)s / 255) * (uint16_t)v / 255;
	uint8_t r = 0, g = 0, b = 0;
	switch ((h / 60) % 6)
	{
		case 0: r = v; g = t; b = p; break;
		case 1: r = q; g = v; b = p; break;
		case 2: r = p; g = v; b = t; break;
		case 3: r = p; g = q; b = v; break;
		case 4: r = t; g = p; b = v; break;
		case 5: r = v; g = p; b = q; break;
	}
	return (rgb_color){.red = r, .green = g, .blue = b};
}

/// Mixes 2 colors with a bias
/// @param a an RGB color
/// @param b an RGB color
/// @param bias the bias goes from 0 to 1
rgb_color
rgb_mix(
	rgb_color a,
	rgb_color b,
	float bias
) {
	return (rgb_color){
		.r = a.r * bias + b.r * (1.0 - bias),
		.g = a.g * bias + b.g * (1.0 - bias),
		.b = a.b * bias + b.b * (1.0 - bias),
	};
}

/// Inits the ports for the LEDs
void
led_init()
{
	// Sets pin low then sets it to output mode
	LED_port_data &= ~(1 << LED_pin_data);
	LED_ddr_data |= (1 << LED_pin_data);
	LED_port_clock &= ~(1 << LED_pin_clock);
	LED_ddr_clock |= (1 << LED_pin_clock);
}

/// Bit bang byte sending
/// @param b the byte to send to the LEDs
void
led_send_byte(uint8_t b)
{
	led_send_bit(b >> 7);
	led_send_bit(b >> 6);
	led_send_bit(b >> 5);
	led_send_bit(b >> 4);
	led_send_bit(b >> 3);
	led_send_bit(b >> 2);
	led_send_bit(b >> 1);
	led_send_bit(b >> 0);
}

/// Sends the start of the frame to the LEDs
void
led_start_frame()
{
	led_init();
	led_send_byte(0);
	led_send_byte(0);
	led_send_byte(0);
	led_send_byte(0);
}

/// Sends the end of the frame to the LEDs
/// @param leds the number of leds
void
led_end_frame(uint16_t leds)
{
	led_send_byte(0xFF);
	for (uint16_t i = 0; i < 5 + leds / 16; ++i){
		led_send_byte(0xFF);
	}
	led_init();
}

/// Writes a full frame to the LEDs
/// @param colors the color table where the first index is the first LED
/// @param count the size of the color table
/// @param brightness the global brightness for all the LEDs
void
led_write(
	rgb_color *colors,
	uint16_t count,
	uint8_t brightness
) {
	led_start_frame();
	for (uint16_t i = 0; i < count; ++i)
	{
		led_send_byte(0b11100000 | brightness);
		led_send_byte(colors[i].blue);
		led_send_byte(colors[i].green);
		led_send_byte(colors[i].red);
	}
	led_end_frame(count);
}
