#include <Arduino.h>
#include "HID-Project.h"

extern "C" { 
	typedef uint16_t kb_keycode;

	void kb_init() {
		NKROKeyboard.begin();
	}

	void kb_end() {
		NKROKeyboard.end();
	}

	void kb_key_add(kb_keycode kc) {
		NKROKeyboard.set(KeyboardKeycode(kc), true);
	};

	void kb_key_rm(kb_keycode kc) {
		NKROKeyboard.set(KeyboardKeycode(kc), false);
	};

	void kb_send() {
		NKROKeyboard.send();
	}

	void kb_flush() {
		NKROKeyboard.releaseAll();
	}
}
