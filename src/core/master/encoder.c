#include <avr/io.h>
#include <avr/interrupt.h>
#include "encoder.h"
#include "serial.h"

void	encoder_init() {
	// Sets ports as inputs
	DDRB &= ~(1<<DDB4 | 1<<DDB5 | 1<<DDB7);
	//ENC_port_push &= ~(1 << ENC_pin_push);
	//ENC_port_plus &= ~(1 << ENC_pin_plus);
	//ENC_port_minus &= ~(1 << ENC_pin_minus);
	// Disable interrupts since we are modifying them
	cli();
	// Enable interrupts for the given pins
	encoder_interrupt_enable();
	// Restores interrupts
	sei();
}

// The state of the push button for the encoder
static bool pushed = false;
// Local states to remember the position of the encoder
// since the last interrupt
static u8	last = 0;
// The delta rotation since the last read
static i16	rot = 0;

void	encoder_update() {
	cli();
	// Read the values for the rotation
	uint8_t minus = ENC_port_minus & (1 << ENC_pin_minus);
	uint8_t plus = ENC_port_plus & (1 << ENC_pin_plus);
	pushed = ENC_port_push & (1 << ENC_pin_push);
	uint8_t state = last & 0b11;
	if (minus) state |= 1 << 2;
	if (plus) state |= 1 << 3;
	last = (state >> 2);
	switch (state) {
		case 0b0001: case 0b0111: case 0b1000: case 0b1110:
			rot++;
			return;
		case 0b0010: case 0b0100: case 0b1011: case 0b1101:
			rot--;
			return;
		case 0b0011: case 0b1100:
			rot += 2;
			return;
		case 0b0110: case 0b1001:
			rot -= 2;
			return;
	}
	sei();
}

i16		encoder_rotation() {
	i16 tmp = rot;
	// Clears the state
	rot = 0;
	// Returns the delta movement
	return tmp;
}

bool	encoder_pushed() {
	return pushed;
}
