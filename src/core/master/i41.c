// ILI9341 driver implementation ATmega32u4
#include "core.h"
#include "i41.h"
#include "encoder.h"
#include "serial.h"

// Quick and Dirty Assert
#define QDA(condition) \
	{if (!(condition)) while (1) {PORTB ^= (1<<PB0); _delay_ms(500);}}

// Raw memory write viewport coordinates
// Starts as full screen
static u16  scrx1 = 0;              // Left offset of viewport
static u16  scry1 = 0;              // Top offset of viewport
static u16  scrx2 = SCREEN_WIDTH;   // Offset of first column outside of viewport
static u16  scry2 = SCREEN_HEIGHT;  // Offset of first line outside of viewport
static u16  scroll_pointer = 0;
static u16  scroll_height = 320;
u8          g_backlight = DEFAULT_BACKLIGHT; // Backlight intensity (PWM duty cycle)

void i41_write_command(u8 command) {
	while (!(SPSR & 1<<SPIF));
	PORT_DC &= ~(1 << BIT_DC); // Command packet
	SPDR = command;	// Packet starts being transmitted.
}

void i41_write_data(u8 data) {
	while (!(SPSR & 1<<SPIF));
	PORT_DC |= 1 << BIT_DC; // Data packet
	SPDR = data;
}

u8 i41_read_data(void) {
	u8 result;

	while (!(SPSR & 1<<SPIF));
	PORT_DC |= 1 << BIT_DC; // Data packet
	SPDR = 0x00;

	while (!(SPSR & 1<<SPIF));
	SPDR = 0x00; // Wasted transmission but SPIF has to be set for the next one.
	result = SPDR;
	return (result);
}

void i41_power_on(void) {
	// SPI initialization
	DDR(PORT_CS) |= 1 << BIT_CS;
	DDR(PORT_RST) |= 1 << BIT_RST;
	DDR(PORT_DC) |= 1 << BIT_DC;

	// TODO: Implement actual backlight pwm control, not this.
	//PORT_BL |= 1 << BIT_BL;
	// NOTE: Actual backlight control
	// NOTE: Interrupt dependent code. See TEMP and 16-bit registers in dtsheet
	ICR1 = 1023; // PWM period
	/*
	*/
	OCR1B = 0;
	// WGM1[3..0] = 0b1110 (Fast PWM with TOP=ICR1)
	TCCR1A = 1<<WGM11 | 0<<WGM10;
	TCCR1B = 1<<WGM12 | 1<<WGM13;
	// COM1B[1..0] = 0b10 (Clear OC1B on compare, set at TOP)
	TCCR1A |= 1<<COM1B1;
	// Enable output of OC1B state on relevant pin
	DDR(PORT_BL) |= 1 << BIT_BL;
	// CS1[2..0] = 0b001 (/1 no prescaling)
	TCCR1B |= 0<<CS12 | 0<<CS11 | 1<<CS10;

	// Slave Select can really just stay low.
	PORT_CS &= ~(1 << BIT_CS);
	// Init SPI pins SS, SCL, MOSI
	DDRB |= 1 << DDB0 | 1 << DDB1 | 1 << DDB2;
	// SPI speed double
	SPSR |= 1 << SPI2X;
	// SPI enabled as master
	SPCR = 1 << SPE | 1 << MSTR;
	// Reset pulse duration
	_delay_ms(10);
	PORT_RST |= 1 << BIT_RST;
	_delay_ms(120);

	// Send a NOP so SPIF will be eventually set like we're expecting it to be
	// before we can write the next packet in SPDR.
	PORT_DC &= ~(1 << BIT_DC);
	SPDR = 0x00; // NOP

	i41_write_command(0xB5); // ---- PRCTR (Blanking Porch control) -
	i41_write_data(0x02); // [6..0]:VFP
	i41_write_data(0x02); // [6..0]:VBP
	i41_write_data(0x0A); // [4..0]:HFP
	i41_write_data(0x14); // [4..0]:HBP
	// Set frame rate for normal mode
	i41_write_command(0xB1); // FRCTR1
	i41_write_data(0x00); // [1..0]:DIVA
	i41_write_data(0x1B); // [4..0]:RTNA
	// Set to BGR and Y inverted (MX incorrect and inverted on this screen)
	i41_write_command(0x36); // ---- MADCTL (Memory Access Control -- 
	//i41_write_data(0x48); // [7]MY, [6]:MX, [5]:MV, [4]:ML, [3]:BGR, [2]:MH
	i41_write_data(0x88); // [7]MY, [6]:MX, [5]:MV, [4]:ML, [3]:BGR, [2]:MH
	// Set to 16bit instead of 18bit RGB in MCU (ie: SPI) interface.
	i41_write_command(0x3A); // ---- PIXSET / COLMOD ----------------
	i41_write_data(0x65); // [6..3]:DPI, [2..0]:DBI

	// Gamma stolen from https://github.com/cbm80amiga/ILI9341_Fast

	i41_write_command(0xE0); // ---- PGAMCTRL (+ Gamma Control) -----
	i41_write_data(0x0F); i41_write_data(0x31); i41_write_data(0x2B);
	i41_write_data(0x0C); i41_write_data(0x0E); i41_write_data(0x08);
	i41_write_data(0x4E); i41_write_data(0xF1); i41_write_data(0x37);
	i41_write_data(0x07); i41_write_data(0x10); i41_write_data(0x03);
	i41_write_data(0x0E); i41_write_data(0x09); i41_write_data(0x00);

	i41_write_command(0xE1); // ---- NGAMCTRL (- Gamma Control) -----
	i41_write_data(0x00); i41_write_data(0x0E); i41_write_data(0x14);
	i41_write_data(0x03); i41_write_data(0x11); i41_write_data(0x07);
	i41_write_data(0x31); i41_write_data(0xC1); i41_write_data(0x48);
	i41_write_data(0x08); i41_write_data(0x0F); i41_write_data(0x0C);
	i41_write_data(0x31); i41_write_data(0x36); i41_write_data(0x0F);

	i41_write_command(0x11); // ---- SLPOUT -------------------------
	_delay_ms(5);
}

void i41_slpin(void) {
	i41_write_command(0x10); // SLPIN
	_delay_ms(120);
}

// Beware: Wait 120ms before a SLPIN
void i41_slpout(void) {
	i41_write_command(0x11); // SLPOUT
	_delay_ms(5);
}

void i41_dispoff(void) {
	i41_write_command(0x28); // DISPOFF
	_delay_ms(34);
}

void i41_dispon(void) {
	i41_write_command(0x29); // DISPON
	_delay_ms(34);
}

void i41_scroll_area(u16 tfa, u16 vsa, u16 bfa) {
	i41_write_command(0x33); // VSCRDEF (Vertical scrolling definition)
	i41_write_data(tfa >> 8);
	i41_write_data(tfa & 0xff);
	i41_write_data(vsa >> 8);
	i41_write_data(vsa & 0xff);
	i41_write_data(bfa >> 8);
	i41_write_data(bfa & 0xff);
	scroll_height = vsa;
}

void i41_scroll(u16 vsp) {
	scroll_pointer = vsp;
	vsp = (SCREEN_HEIGHT - vsp) % SCREEN_HEIGHT;
	i41_write_command(0x37); // VSCRSADD (Vertical Scrolling Start Address)
	i41_write_data(vsp >> 8);
	i41_write_data(vsp & 0xff);
}

void i41_viewport(u16 x1, u16 y1, u16 x2, u16 y2) {
	i41_write_command(0x2A); // CASET (Column Address set)
	i41_write_data(x1 >> 8);
	i41_write_data(x1 & 0xff);
	i41_write_data(x2 >> 8);
	i41_write_data(x2 & 0xff);
	i41_write_command(0x2B); // PASET (Page Address set)
	i41_write_data(y1 >> 8);
	i41_write_data(y1 & 0xff);
	i41_write_data(y2 >> 8);
	i41_write_data(y2 & 0xff);
	scrx1 = x1;
	scry1 = y1;
	scrx2 = x2;
	scry2 = y2;
}

void i41_fill_screen(u16 rgb) {
	u16 x, y;

	i41_write_command(0x2C); // RAMWR
	while (!(SPSR & 1<<SPIF)) ; // Wait for D/C to be latched before changing it
	y = scry2 - scry1;
	x = scrx2 - scrx1;
	PORT_DC |= 1 << BIT_DC; // Upcoming data packets
	while (y > 0) {
		while (x > 0) {
			while (!(SPSR & 1<<SPIF)) ;
			SPDR = rgb >> 8;
			while (!(SPSR & 1<<SPIF)) ;
			SPDR = rgb & 0xff;
			x--;
		}
		x = scrx2 - scrx1 + 1;
		y--;
	}
	i41_write_command(0x00); // NOP to signal end of RAM Write
}

/*
** Fill the screen with 2D color gradiant
*/

void i41_fill_screen_with_gradiant(void) {
	u16 x, y;
	u16 w, h;
	u16 green;
	u16 rgb;
	u8	tmp;

	(void) rgb;
	(void) green;
	(void) tmp;

	w = scrx2 - scrx1;
	h = scry2 - scry1;
	i41_write_command(0x2C); // RAMWR
	// Careful: D/C is latched with the last bit transmitted
	while (!(SPSR & 1<<SPIF)) ; // Wait for D/C to be latched before changing it
	PORT_DC |= 1 << BIT_DC; // Upcoming packets will be data.
	for (y = 0; y < h; y++) {
		for (x = 0; x < w; x++) {
			green = x*64/w > y*64/h ? y*64/h*4 : x*64/w*4;
			rgb = RGBto565(x * 32 / w * 8, green, y * 32 / h * 8);
			while (!(SPSR & 1<<SPIF)) ;
			SPDR = MSB(rgb);
			while (!(SPSR & 1<<SPIF)) ;
			SPDR = LSB(rgb);
		}
	}
	i41_write_command(0x00); // NOP (to signal end of RAMWR)
}

/*
** Fills the rectangle from `x1` to `x2` included and from `y1` to `y2` included
** with `color`
*/

void i41_fill_rect(u16 x1, u16 y1, u16 x2, u16 y2, u16 color) {
	u16 x, y;
	u16 tmpx1, tmpx2, tmpy1, tmpy2;

	tmpx1 = scrx1;
	tmpy1 = scry1;
	tmpx2 = scrx2;
	tmpy2 = scry2;
	i41_viewport(x1, y1, x2, y2);
	i41_write_command(0x2C); // RAMWR
	while (!(SPSR & 1<<SPIF)) ; // Wait for D/C to latch
	PORT_DC |= 1 << BIT_DC;
	for (y = y2 - y1 + 1; y > 0; --y) {
		for (x = x2 - x1 + 1; x > 0; --x) {
			while (!(SPSR & 1<<SPIF)) ;
			SPDR = color>>8;
			while (!(SPSR & 1<<SPIF)) ;
			SPDR = color&0xff;
		}
	}
	i41_write_command(0x00); // NOP (to signal end of RAMWR)
	i41_viewport(tmpx1, tmpy1, tmpx2, tmpy2);
}

/*
** Dirty draw rectangle function. Doesn't check out of bound coordinates.
** @param thickness >1 for outside, <-1 for inside. -1, 0 and +1 are the same
*/

void i41_draw_rect(u16 x1, u16 y1, u16 x2, u16 y2, u16 color, i16 thickness) {
	if (thickness == 0)
		thickness = 1; // Assume caller meant thickness of 1 by default.
	u16 thick = ABS(thickness);

	if (thickness < 0) {
		x2 += thickness + 1;
		y2 += thickness + 1;
	}
	else {
		x1 -= thickness - 1;
		y1 -= thickness - 1;
	}
	i41_fill_rect(x1, y1, x2 + thick - 1, y1 + thick - 1, color);
	i41_fill_rect(x1, y1 + thick, x1 + thick - 1, y2 - 1, color);
	i41_fill_rect(x2, y1 + thick, x2 + thick - 1, y2 - 1, color);
	i41_fill_rect(x1, y2, x2 + thick - 1, y2 + thick - 1, color);
}

void i41_set_backlight(u8 backlight) {
	// Logarithmic scale approximation
	OCR1B = (((1<<10)-1) >> (10 - backlight / 4))
		+ (1 << (backlight / 4)) * (backlight % 4) / 4; // PWM duty cycle
}

/*
** -------------- **
** Text rendering **
** -------------- **
*/

/*
** Retrieves dimensions of rectangle contained in flash at `addr`.
** Retrieves width-1 in `w` and height-1 in `h`.
*/

static void i41_rre_read_16b(u16 *addr, u16 *x, u16 *y, u16 *w, u16 *h) {
	u16 data;
	
	data = pgm_read_word(addr);
	*x = (data >> 0) & 0x0f;
	*y = (data >> 4) & 0x0f;
	*w = ((data >> 8) & 0x0f) + 1;
	*h = ((data >> 12) & 0x0f) + 1;
}

/*
** Get width of character `c` in `font` encoded with 16-bit rectangles
*/

u16 i41_rre_char_width_16b(const RRE_Font *font, u8 c) {
	u16 x, y, w, h;
	u16 index, index_end;

	index = pgm_read_word(font->offs + c - font->firstCh);
	index_end = pgm_read_word(font->offs + c - font->firstCh + 1);
	if (index >= index_end)
		return (c == ' ' ? font->wd * 4 / 9 : 0);
	// Non legacy fonts' last rectangle is always rightmost.
	i41_rre_read_16b((u16 *)font->rects + (index_end - 1), &x, &y, &w, &h);
	return (x + w);
}

/*
** Draws character `c` at offset (x,y) with `font` and `fg`/`bg` as
** foreground/background colors
** NOTE: Doesn't check for viewport or screen boundaries
** @return Width of drawn character
*/

u16 i41_draw_character(const RRE_Font *font,
		u16 scale, u16 fg, u16 bg, u16 x, u16 y, u8 c) {
	u16 s = scale;
	u16 xr, yr, w, h;
	u16 start, end;
	u16	width;

	if (c < font->firstCh || c > font->lastCh)
		return (0);
	start = pgm_read_word(font->offs + c - font->firstCh);
	end = pgm_read_word(font->offs + c - font->firstCh + 1);
	width = s * i41_rre_char_width_16b(font, c);
	i41_fill_rect(x, y, x + width - 1, y + font->ht*s - 1, bg);

	while (start < end) {
		i41_rre_read_16b((u16 *)font->rects + start, &xr, &yr, &w, &h);
		i41_fill_rect(x+xr*s, y+yr*s, x+(xr+w)*s-1, y+(yr+h)*s-1, fg);
		start++;
	}
	return (width);
}

/*
** Same as previous function. Faster but much more preprocessing.
** TODO: This can be optimized by using bits instead of u16.
*/

u16 i41_blit_character(const RRE_Font *font, u16 fg, u16 bg, u16 x, u16 y, u8 c) {
	const u16 bufw = 8; // Rasterization buffer's width
	const u16 bufh = 12; // Rasterization buffer's height
	u16 buf[bufw * bufh]; // Raster buffer. Only `width`*`font->ht` will be used
	u16 rx, ry, rw, rh; // Dimensions of one rectangle
	u16 start, end; // Offset interval of rectangles in a glyph
	u16 width; // Width of the character
	u16	ix, iy; // Iterators of the rasterization
	u8 i;

	if (c < font->firstCh || c > font->lastCh)
		return (0);
	start = pgm_read_word(font->offs + c - font->firstCh);
	end = pgm_read_word(font->offs + c - font->firstCh + 1);
	width = i41_rre_char_width_16b(font, c);
	// Fill background.
	for (i = 0; i < width * font->ht; i++)
		buf[i] = bg;
	while (start < end) {
		i41_rre_read_16b((u16 *)font->rects + start, &rx, &ry, &rw, &rh);
		// Fill this rectangle of index `start`
		for (iy = ry; iy < ry+rh; iy++) {
			for (ix = rx; ix < rx+rw; ix++) {
				buf[iy * width + ix] = fg;
			}
		}
		start++;
	}
	// Perform area fill (same as i41_fill_rect).
	i41_viewport(x, y, x + width - 1, y + font->ht - 1);
	i41_write_command(0x2C); // RAMWR
	while (!(SPSR & 1 << SPIF)) ;
	PORT_DC |= 1 << BIT_DC;
	for (i = 0; i < width * font->ht; i++) {
		while (!(SPSR & 1 << SPIF)) ;
		SPDR = buf[i] >> 8;
		while (!(SPSR & 1 << SPIF)) ;
		SPDR = buf[i] & 0xff;
	}
	i41_write_command(0x00); // NOP
	return (width);
}

/**
** Simple unoptimized draw text using i41_draw_character.
** NOTE: Viewport has to be wider and higher than max character width
** @param cursor Top left offset to start drawing at
** @param font RRE font to draw the string with
** @param scale Drawn size multiplier
** @param monospaced Forces monospacing if non-zero
** @param fg Foreground in 16-bit RGB
** @param bg Background in 16-bit RGB
** @param x Left side offset
** @param y Top offset
** @param str String to draw
** @return Top left offset of where to draw next
*/

t_cursor i41_draw_string(t_cursor cursor, const RRE_Font *font, u16 scale,
                         u8 monospaced, u16 fg, u16 bg, const char *str) {
	u16 spacing = 1 * scale;
	u16 width, height;
	u16 actual_width; // Character width without monospacing

	height = font->ht * scale;
	for (; *str != '\0'; str++) {
		// Skip characters unsupported by font
		if (*str < font->firstCh || *str > font->lastCh)
			width = 0;
		else
			width = monospaced ? font->wd * scale :
				scale * i41_rre_char_width_16b(font, *str);
		// Handle line wrap and newline
		if (cursor.x + width - 2 > scrx2 || *str == '\n' || *str == '\r') {
			if (cursor.x < scrx2) { // Newline padding
				i41_fill_rect(cursor.x, cursor.y, scrx2,
					cursor.y + font->ht * scale - 1, bg);
			}
			cursor.x = scrx1;
			cursor.y += height;
			// This will break if scroll_height is not a multiple of height
			if ((scroll_height + scroll_pointer - cursor.y)
						% scroll_height < height) {
				scroll_pointer = (cursor.y + height) % scroll_height;
				i41_scroll(scroll_pointer);
			}
			if (cursor.y + height - 1 > scry2)
				cursor.y = scry1;
		}
		if (*str < font->firstCh || *str > font->lastCh)
			continue ;
		// Draw character
		actual_width = i41_draw_character(
			font, scale, fg, bg,
			cursor.x, cursor.y,
			(u8)*str);
		// Additional background padding for monospaced font
		if (actual_width < width)
			i41_fill_rect(
				cursor.x + actual_width,
				cursor.y,
				cursor.x + width - 1,
				cursor.y + font->ht * scale - 1,
				bg);
		cursor.x += width;
		// Spacing between letters (Only if not monospaced)
		if (!monospaced && cursor.x < scrx2) {
			i41_fill_rect(
				cursor.x,
				cursor.y,
				CAP(cursor.x + spacing - 1, scrx2 - 1),
				cursor.y + font->ht * scale - 1,
				bg);
			cursor.x = CAP(cursor.x + spacing, scrx2);
		}
	}
	return (cursor);
}

t_cursor i41_putnumber(t_cursor cursor, const RRE_Font * font, u16 scale,
                         u8 monospaced, u16 fg, u16 bg, i16 number) {
	char out[7]; // 1 sign + max 5 digits + 1 '\0'
	u8 negative = (number < 0 ? 1 : 0);
	u8 i;

	out[6] = '\0'; // We're building a NUL terminated string
	for (i = 5; number != 0; number /= 10, i--)
		out[i] = '0' + (negative ? -(number % 10) : number % 10);
	if (i == 5)
		out[5] = '0';
	else if (negative)
		out[i] = '-';
	else
		i++; // Next character is first character instead of this one being '+'
	// At this point i is always the index of the first character
	return (i41_draw_string(cursor, font, scale, monospaced, fg, bg, out + i));
}

const u16   menu_margin_top = 13;
const u16   menu_margin_left = 9;
const u8	menu_scale = 2;

static void **menu = NULL;     // Current menu
static void (*menu_func)(void *) = NULL; // Function to call instead of menu
static void *menu_func_data = NULL; // Data to call menu function hook with
static const RRE_Font *menu_font = NULL; // Menu font
static i16  encoder_value = 0; // cumulated_encoder_rotation % 4 so that it gets
                              // reset when a new stable position is reached.
static u8   push_state = 0;   // Whether encoder was pushed last time we checked
static u8   menu_cursor = 0;  // Which menu entry is highlighted
static t_cursor tips[10];     // Top right corners of entry texts
static const char *menu_strings[10]; // Strings for each entry
static u8   menu_drawn = 0;   // Set to 1 if menu is currently drawn
static u8   cursor_drawn = 0; // 1 when cursor is drawn
static u8   entries_count;    // Number of menu entries drawn on screen

void i41_init_menu(void *new_menu, const RRE_Font *new_font) {
	menu = new_menu;
	menu_font = new_font;
	menu_cursor = 0;
}

static void erase_focus_cursor(void) {
	t_cursor    tip; // Top right end of selected text
	t_cursor    btl; // Bottom left end of selected text
	tip = tips[menu_cursor];
	btl.x = menu_margin_left;
	btl.y = tip.y + menu_font->ht * 2;
	// Rectangle around entry, 2px thick, 1px padding
	i41_draw_rect(btl.x - 2, tip.y - 2, tip.x + 1, btl.y + 1, 0x0000, 2);
	cursor_drawn = 0;
}

static void draw_focus_cursor(void) {
	t_cursor    tip; // Top right end of selected text
	t_cursor    btl; // Bottom left end of selected text
	tip = tips[menu_cursor];
	btl.x = menu_margin_left;
	btl.y = tip.y + menu_font->ht * 2;
	// Rectangle around entry, 2px thick, 1px padding
	i41_draw_rect(btl.x - 2, tip.y - 2, tip.x + 1, btl.y + 1, 0xffff, 2);
	cursor_drawn = 1;
}

void i41_process_menu(void) {
	// Cursor for drawing text
	t_cursor    cursor = {menu_margin_left, menu_margin_top};
	u8          i = 0; // Index of current entry
	const char  *str;

	/*
	if (encoder_pushed())
		i41_draw_string((t_cursor){40, 290}, menu_font, 2, 1,
			0xffff, 0x0000, "Encoder push");
	else
		i41_draw_string((t_cursor){40, 290}, menu_font, 2, 1,
			0xffff, 0x0000, "Encoder nosh");
	*/
	static u8 blorg = 0;
	i41_fill_rect(0, 300, 19, 319, blorg ? 0xf800 : 0x001f);
	blorg = !blorg;
	if (menu_func != NULL) {
		menu_func(menu_func_data);
		return ;
	}
	if (menu_drawn == 0) {
		str = menu[0];
		while (*str != '\0') {
			menu_strings[i] = str;
			tips[i] =
				i41_draw_string(cursor, menu_font, 2, 0, 0xffff, 0x0000, str);
			while (*++str != '\0') ;
			++str; // From string end to start of next string (or '\0' if last)
			if (i == menu_cursor) {
				// Highlight selected entry
			}
			cursor.x = menu_margin_left;
			cursor.y += menu_font->ht * 2 + 4;
			i++;
		}
		entries_count = i;
		menu_drawn = 1;
	}

	// Entry cursor logic
	encoder_value += encoder_rotation();
	i16 rem = ((i16)menu_cursor + encoder_value / 4) % (i16)entries_count;
	i = (u8)(rem < 0 ? rem + entries_count : rem); // Modulo from remainder
	encoder_value = encoder_value % 4; // Substract reached stable positions
	if (cursor_drawn && i != menu_cursor) {
		// Erase current entry cursor
		erase_focus_cursor();
		menu_cursor = i;
	}
	if (!cursor_drawn) {
		// Draw new entry cursor
		draw_focus_cursor();
	}

	// Selection logic
	u8 new_push_state = encoder_pushed();
	if (!push_state && new_push_state) {
		// See main.c for explaination
		if (menu[1 + menu_cursor * 2] == NULL) {
			// Entry doesn't refer to function hook
			if (menu[1 + menu_cursor * 2 + 1] == NULL)
				; // Stub entry, don't do anything
			else { // Entry refers to sub-menu
				menu = menu[1 + menu_cursor * 2 + 1];
				menu_drawn = 0;
				cursor_drawn = 0;
				i41_fill_screen(0x0000);
			}
		}
		else { // Entry refers to function hook
			menu_func = menu[1 + menu_cursor * 2];
			menu_func_data = menu[1 + menu_cursor * 2 + 1];
			// NOTE: Neither menu nor cursor will be erased
		}
	}
	push_state = new_push_state;
}

/*
** Partial draw gauge
** NOTE: Happens to work because screen width doesn't exceed 8 bits so length
** calculation multiplication can't overflow 16 bits.
** Draws only the transition from oldval to val to be more responsive
*/

static void draw_gauge_transition(
		u8 entry, u8 max, u8 oldval, u8 val, u16 color) {
	t_cursor    tip; // Top right end of selected text
	t_cursor    btl; // Bottom left end of selected text

	// Rectangle over entry text with 2 px outside padding
	// Reuse margin_left for right margin
	tip.x = SCREEN_WIDTH - menu_margin_left - 3;
	tip.y = tips[entry].y + 2;
	btl.x = menu_margin_left + 2;
	btl.y = tip.y + menu_font->ht * 2 - 3;
	// Calculation of filled part of the gauge
	u16 len = tip.x - btl.x + 1;
	u16 oldlen = len * (u16)oldval / (u16)max;
	len = len * (u16)val / (u16)max;
	if (len < oldlen) // Erase some of the gauge
		i41_fill_rect(btl.x+len, tip.y, btl.x+oldlen-1, btl.y-2, 0x0000);
	else if (len > oldlen) // Fill some of the gauge
		i41_fill_rect(btl.x+oldlen, tip.y, btl.x+len-1, btl.y-2, color);
}

void i41_menu_slider_hook(t_slider_data *data) {
	t_cursor    tip; // Top right end of dialog (grid coordinates)
	t_cursor    btl; // Bottom left end of dialog (grid coordinates)
	static u8   gauge_value;
	i16          rot;

	tip.x = SCREEN_WIDTH - menu_margin_left;
	tip.y = tips[menu_cursor].y;
	btl.x = menu_margin_left;
	btl.y = tip.y + menu_font->ht * 2;

	if (cursor_drawn)
		erase_focus_cursor();

	// First execution after dialog is displayed
	static u8   dialog_drawn = 0;
	if (!dialog_drawn) {
		rot = 0;
		i41_fill_rect(tip.x, tip.y, SCREEN_WIDTH - 1, btl.y - 1, 0x0000);
		// Fill gauge tile and erase padding
		i41_draw_rect(btl.x, tip.y, tip.x - 1, btl.y - 1, 0xffff, 1);
		i41_draw_rect(btl.x + 1, tip.y + 1, tip.x - 2, btl.y - 2, 0x0000, 1);
		// Fill black rectangle with 2px outside padding ie. empty gauge
		i41_fill_rect(btl.x + 2, tip.y + 2, tip.x - 3, btl.y - 3, 0x0000);
		// Draw transition from empty gauge to gauge with desire value
		draw_gauge_transition(menu_cursor,
			data->max, 0, *data->value, *data->color);
		gauge_value = *data->value;
		dialog_drawn = 1;
	}

	rot = encoder_rotation();
	// Maintain encoder value to avoid drift
	encoder_value = (encoder_value + rot) % 4;

	if (rot != 0) { // Update gauge
		*data->value = CLAMP((i16)*data->value + rot, 0, 40);
		data->callback(*data->value);
		draw_gauge_transition(menu_cursor,
				data->max, gauge_value, *data->value, *data->color);
		gauge_value = *data->value;
		/*
		t_cursor cursor = {30, 299};
		i41_draw_string(cursor, menu_font, 1, 1, 0x0000, 0x0000, "00");
		i41_putnumber(cursor, menu_font, 1, 1, 0xffff, 0x0000, g_backlight);
		cursor = (t_cursor){80, 299};
		i41_draw_string(cursor, menu_font, 1, 1, 0x0000, 0x0000, "0000");
		i41_putnumber(cursor, menu_font, 1, 1, 0xffff, 0x0000, OCR1B);
		i16 tmp = (((1<<10)-1) >> (10 - g_backlight / 4))
			+ (1 << (g_backlight / 4)) * (g_backlight % 4) / 4;
		cursor = (t_cursor){130, 299};
		i41_draw_string(cursor, menu_font, 1, 1, 0x0000, 0x0000, "0000");
		i41_putnumber(cursor, menu_font, 1, 1, 0xffff, 0x0000, tmp);
		*/
	}

	// Confirmation logic
	u8 new_push_state = encoder_pushed();
	if (!push_state && new_push_state) {
		// Erase everything and exit to calling menu
		//i41_fill_screen(0x0000);
		i41_fill_rect(btl.x, tip.y, tip.x - 1, btl.y - 1, 0x0000);
		// Redraw menu entry covered by dialog
		// Lazy: reuse tip for that
		tip.x = menu_margin_left;
		i41_draw_string(tip, menu_font, 2, 0, 0xffff, 0x0000,
				menu_strings[menu_cursor]);
		dialog_drawn = 0;
		cursor_drawn = 0;
		menu_func = NULL;
		menu_func_data = NULL;
	}
	push_state = new_push_state;
}

void i41_menu_sleep(void *data) {
	(void)data;
	static u8 in_sleep = 0;
	if (!in_sleep) {
		TCCR1A &= ~(1<<COM1B1 | 1<<COM1B0);
		PORTB &= ~(1<<PB6);
		in_sleep = 1;
	}

	u8 new_push_state = encoder_pushed();
	if (!push_state && new_push_state) {
		TCCR1A |= 1<<COM1B1 | 0<<COM1B0;
		in_sleep = 0;
		menu_func = NULL;
	}
	push_state = new_push_state;
}

void i41_menu_terminal(void *dialog_data)
{
	static bool first_call = true;
	static t_cursor cursor = {0, 0};
	uint8_t new_characters = serial_available();

	// Exit logic
	u8 new_push_state = encoder_pushed();
	if (!push_state && new_push_state) {
		/* // Erase everything and exit to calling menu */
		/* //i41_fill_screen(0x0000); */
		/* i41_fill_rect(btl.x, tip.y, tip.x - 1, btl.y - 1, 0x0000); */
		/* // Redraw menu entry covered by dialog */
		/* // Lazy: reuse tip for that */
		/* tip.x = menu_margin_left; */
		/* i41_draw_string(tip, menu_font, 2, 0, 0xffff, 0x0000, */
		/* 		menu_strings[menu_cursor]); */
		i41_fill_screen(0x0000);
		menu_drawn = 0;
		cursor_drawn = 0;
		first_call = 1;
		menu_func = NULL;
		i41_scroll(0);
		return ;
	}
	push_state = new_push_state;
	if (first_call) {
		i41_fill_screen(0x0000);
		cursor = (t_cursor) {0,0};
	}
	first_call = false;
	if (!new_characters)
		return;

	char str[65] = { 0 };
	for (u8 i = 0; i < new_characters; i++) {
		str[0] = serial_read();
		if (*str == '\x7f') {
			i41_fill_rect(
				MIN(0, cursor.x - menu_font->wd),
				cursor.y,
				cursor.x - 1,
				cursor.y + menu_font->ht - 1,
				0x0000);
			cursor.x = MIN(0, cursor.x - menu_font->wd);
			continue;
		}
		cursor = i41_draw_string(cursor, menu_font, 1, 1, 0xffff, 0x000, str);
	}
}

void i41_init(void) {
	const u16 font_height = 12;
	const u16 font_scale = 1;
	const u16 scroll_margin = SCREEN_HEIGHT % (font_height * font_scale);
	i41_power_on();
	i41_dispon();
	i41_fill_screen(0x0000);
	i41_scroll_area(scroll_margin, SCREEN_HEIGHT - scroll_margin, 0);
	i41_set_backlight(DEFAULT_BACKLIGHT);
}
