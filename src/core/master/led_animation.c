#include "core.h"
#include "sk9822.h"
#include "animations.h"
#include "keyboard.h"
#include "serial.h"
#include "time.h"
#include "eeprom.h"
#include "twi.h"

/// LEDs colors strip
static rgb_color			colors[ANIM_led_count];
/// buffer map as float to be used with different animations
/// There is 2 in the first dimention as we split the keyboard in 2
static float				buffer[2][ANIM_key_width][ANIM_key_height];
/// The animation's config which is persistant
static animation_config		config;

/// Gets the colors at the x y coordinates considering the layout for the front
/// Note: doesn't check for valid range
/// @param x the x coordinate of the strip
/// @param y the y coordinate of the strip
/// @return Returns the color reference on the LED strip
static inline rgb_color*	color_at_front(u8 x, u8 y) {
	return &colors[x + y * ANIM_key_width];
}

/// Gets the colors at the x y coordinates considering the layout for the back
/// Note: doesn't check for valid range
/// @param x the x coordinate of the strip
/// @param y the y coordinate of the strip
/// @return Returns the color reference on the LED strip
static inline rgb_color*	color_at_back(u8 x, u8 y) {
	return &colors[
		ANIM_key_height * ANIM_key_width // Front matrix
		+ ((y > 0) ? x : ANIM_back_width - x - 1) // x
		+ y * ANIM_back_width // y
	];
}

static unsigned long timer = 0;
static unsigned long delta = 0;

/// Initialize the delta time to current time
static void init_delta() {
    timer = micros();
}

/// Updates the delta time to the current time
/// Use this before calling get_delta
static void update_delta() {
    unsigned long new_timer = micros();
    delta = new_timer - timer;
    timer = new_timer;
}

/// Used to get the delta time (You need to call update_delta and init_delta)
/// @return The delta time in ms
static float get_delta() {
    return delta * 0.001; // divide by 1000 to get millis
}

void animate_color_wave(
	float lightness[ANIM_key_width][ANIM_key_height],
	float seed,
	float time,
	hsv_color primary,
	hsv_color accent,
	kb_keycode report[ANIM_key_width][ANIM_key_height]
) {
	(void)primary, (void)accent, (void)report;
	const float pattern_width = 10.0;
	const float pattern_height = 10.0;
	const unsigned long ms = millis();

	// Color wave for backlight
	for (u8 x = 0; x < ANIM_key_width; x++) {
		for (u8 y = 0; y < ANIM_key_height; y++) {
			// Direction and width of the rainbow,
			// The x * y term makes it curve a bit.
			uint8_t p = ((float)ms * .05 + seed * 40) + (x * pattern_width + y * pattern_height);
			*color_at_front(x, y) = hsv_to_rgb(((float)p * 360.0) / 256.0, 255, 255 * 1.);
		}
	}
	// Color wave for underglow
	for (u8 x = 0; x < ANIM_back_width; x++) {
		for (u8 y = 0; y < ANIM_back_height; y++) {
			// Direction and width of the rainbow,
			// The x * y term makes it curve a bit.
			u8 p = ((float)ms * .05 + seed * 40) + (x * pattern_height + y * pattern_width);
			*color_at_back(x, y) = hsv_to_rgb(((float)p * 360.0) / 256.0, 255, 255 * 1.);
		}
	}
}

void animate_breathing(
	float lightness[ANIM_key_width][ANIM_key_height],
	float seed,
	float time,
	hsv_color primary,
	hsv_color accent,
	kb_keycode report[ANIM_key_width][ANIM_key_height]
) {
	(void)report;
	const unsigned long ms = millis();
	float t = (sin((float)ms * .002) + 1) / 2;
	float p = exp(-(pow((t-.7)/.4,2.0))/1.4);
	rgb_color front = hsv_to_rgb(primary.h, primary.s, primary.v);
	rgb_color back = hsv_to_rgb(accent.h, accent.s, accent.v);
	front.r *= p;
	front.g *= p;
	front.b *= p;
	back.r *= p;
	back.g *= p;
	back.b *= p;

	// Color wave for backlight
	for (u8 x = 0; x < ANIM_key_width; x++)
		for (u8 y = 0; y < ANIM_key_height; y++)
			*color_at_front(x, y) = front;
	// Color wave for underglow
	for (u8 x = 0; x < ANIM_back_width; x++)
		for (u8 y = 0; y < ANIM_back_height; y++)
			*color_at_back(x, y) = back;
}

void animate_reactive(
	float lightness[ANIM_key_width][ANIM_key_height],
	float seed,
	float time, // Delta time
	hsv_color primary,
	hsv_color accent,
	kb_keycode report[ANIM_key_width][ANIM_key_height]
) {
	// Global fade to black
	for (u8 x = 0; x < ANIM_key_width; x++) {
		for (u8 y = 0; y < ANIM_key_height; y++) {
			lightness[x][y] *= 1.0 - (time / config.duration);
		}
	}

	// If a key is pressed then sets it to the primary color
	for (u8 x = 0; x < ANIM_key_width; x++) {
		for (u8 y = 0; y < ANIM_key_height; y++) {
			if (report[x][y]) {
				lightness[x][y] = 1.0;
			}
			*color_at_front(x, y) = hsv_to_rgb(primary.h, primary.s, primary.v * lightness[x][y]);
		}
	}
	// Background color
	rgb_color back = hsv_to_rgb(accent.h, accent.s, accent.v);
	for (u8 x = 0; x < ANIM_back_width; x++) {
		for (u8 y = 0; y < ANIM_back_height; y++) {
			*color_at_back(x, y) = back;
		}
	}
}

void animate_color_reactive(
	float lightness[ANIM_key_width][ANIM_key_height],
	float seed,
	float time, // Delta time
	hsv_color primary,
	hsv_color accent,
	kb_keycode report[ANIM_key_width][ANIM_key_height]
) {
	(void)primary, (void)accent;
	const float pattern_width = 10.0;
	const float pattern_height = 10.0;
	const unsigned long ms = millis();

	// Global fade to black
	for (u8 x = 0; x < ANIM_key_width; x++) {
		for (u8 y = 0; y < ANIM_key_height; y++) {
			lightness[x][y] *= 1.0 - (time / config.duration);
		}
	}
	// If a key is pressed then sets it to the primary color
	for (u8 x = 0; x < ANIM_key_width; x++) {
		for (u8 y = 0; y < ANIM_key_height; y++) {
			if (report[x][y]) {
				lightness[x][y] = 1.0;
			}
			u8 p = ((float)ms * .05) + (x * pattern_width + y * pattern_height);
			*color_at_front(x, y) = hsv_to_rgb(((float)p * 360.0) / 256.0, 255, 255 * lightness[x][y]);
		}
	}
	// Background color
	for (u8 x = 0; x < ANIM_back_width; x++) {
		for (u8 y = 0; y < ANIM_back_height; y++) {
			u8 p = ((float)ms * .05) + (x * pattern_width + y * pattern_height);
			*color_at_back(x, y) = hsv_to_rgb(((float)p * 360.0) / 256.0, 255, 255);
		}
	}
}

void animate_none(
	float lightness[ANIM_key_width][ANIM_key_height],
	float seed,
	float time, // Delta time
	hsv_color primary,
	hsv_color accent,
	kb_keycode report[ANIM_key_width][ANIM_key_height]
) {
	(void)primary, (void)accent, (void)lightness, (void)seed, (void)time, (void)report;
	for (u8 x = 0; x < ANIM_led_count; x++) {
		colors[x] = (rgb_color){};
	}
}


/// Animation mapping
const led_animation	animations[ANIM_max] = {
	[ANIM_none] = animate_none,
	[ANIM_color_wave] = animate_color_wave,
	[ANIM_breathing] = animate_breathing,
	[ANIM_reactive] = animate_reactive,
	[ANIM_color_reactive] = animate_color_reactive,
};

// void print_config(){
// 	serial_print_str("Magic: ");
// 	serial_print_u8(config.magic);
// 	serial_print_str("\nDuration: ");
// 	serial_print_f(config.duration);
// 	serial_print_str("\nSpeed: ");
// 	serial_print_f(config.speed);
// 	serial_print_str("\nDisplay back: ");
// 	serial_print_u8(config.display_back);
// 	serial_print_str("\nDisplay front: ");
// 	serial_print_u8(config.display_front);
// 	serial_print_str("\nPrimary: ");
// 	serial_print_u8(config.primary.h);
// 	serial_print_str(", ");
// 	serial_print_u8(config.primary.s);
// 	serial_print_str(", ");
// 	serial_print_u8(config.primary.v);
// 	serial_print_str("\nAccent: ");
// 	serial_print_u8(config.accent.h);
// 	serial_print_str(", ");
// 	serial_print_u8(config.accent.s);
// 	serial_print_str(", ");
// 	serial_print_u8(config.accent.v);
// 	serial_print_str("\n");
// }

/// Loads the config or initialiaze it when the eeprom it isn't written yet
void
animate_init()
{
	u8 magic;
	// Reads the magic number from the eeprom to compare
	eeprom_load(&magic, ANIM_config, 1);
	// If eeprom already init
	if (magic != ANIM_magic) {
		// Set default values
		config = (animation_config){
			.magic = ANIM_magic,
			.anim = ANIM_color_wave,
			.speed = 1.0,
			.duration = 500.0,
			.display_back = true,
			.display_front = true,
			.primary = (hsv_color){.h = 180, .s = 200, .v = 255},
			.accent = (hsv_color){.h = 290, .s = 200, .v = 255},
		};
		eeprom_save(&config, ANIM_config, sizeof(config));
	} else {
		eeprom_load(&config, ANIM_config, sizeof(config));
	}
	// Inits delta to be able to animate with delta time
	init_delta();
}

/// Animation wrapper to use as a hub to display an animation on a LED matrix
/// @param anim the animation id to use
/// @param report the matrix report of the key pressed
void
animate_leds(
		matrix_report *report
) {
	// Gets the delta
	update_delta();
	float delta = get_delta();
	// Apply colors given an animation
	animations[config.anim](buffer[0], 0.0, delta * config.speed, config.primary, config.accent, report->left);
	// Write to the LEDs after calculation
	led_write(colors, ANIM_led_count, 10);
	// Calculate for the slave
	animations[config.anim](buffer[1], 1.0, delta * config.speed, config.primary, config.accent, report->right);
	// Then sends it to him
	if (!(twi_write_to(0xb, (u8*)colors, ANIM_led_count * sizeof(rgb_color), true, true))) {
		twi_manage_timeout_flag(true);
	}
}

// User interaction with animations

/// Set the animation variant
/// @param anim the animation to set to (can overflow)
void animate_set_anim(animation anim) {
	if (!(anim >= ANIM_none && anim < ANIM_max))
		return;
	config.anim = anim;
	eeprom_save(&config, ANIM_config, sizeof(config));
}

/// Set the speed multiplier of the animation
/// @param speed the multiplier
void animate_set_speed(float speed) {
	config.speed = speed;
	eeprom_save(&config, ANIM_config, sizeof(config));
}

/// Set the duration duration of interactive animations
/// @param duration the duration in ms
void animate_set_duration(float duration) {
	config.duration = duration;
	eeprom_save(&config, ANIM_config, sizeof(config));
}

/// Set the primary color
/// @param color the color
void animate_set_primary(hsv_color color) {
	config.primary = color;
	eeprom_save(&config, ANIM_config, sizeof(config));
}

/// Set the accent color
/// @param color the color
void animate_set_accent(hsv_color color) {
	config.accent = color;
	eeprom_save(&config, ANIM_config, sizeof(config));
}

/// Gets the config for the animation
/// @return the config
animation_config animate_get_config() {
	return config;
}

/// Gets the config for the animation
/// @return the config
animation animate_get_anim() {
	return config.anim;
}
