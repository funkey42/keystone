#include <avr/io.h>
#include "core.h"
#include "keyboard.h"
#include "serial.h"
#include "time.h"

#include "twi.h"

#if defined(TWI_matrix_address)
#else
#warning "TWI_matrix_address is not defined. Setting to 0x8."
#define TWI_matrix_address 0x8
#endif
#if defined(TWI_freq)
#else
#warning "TWI_freq is not defined. Setting to 1M."
#define TWI_freq 1000000ul
#endif


// Serial debug function to print the state of the matrix
// void print_report(matrix_report *report) {
// 	for (u8 j = 0; j < KB_matrix_height; ++j)
// 	{
// 		serial_print_u8(j);
// 		serial_print_str(" > ");
// 		for (u8 i = 0; i < KB_matrix_width; ++i) {
// 			serial_print_i16(report->states[i][j]);
// 			if (i< KB_matrix_width - 1)
// 				serial_print_str(":");
// 		}
// 		serial_print_str("\n");
// 	}
// 	serial_print_str("\n");
// }

// Default layer that can change with the user input.
static kb_layer default_layer = LAYER_DEFAULT;
static kb_layer current_layer = LAYER_DEFAULT;
static kb_layer previous_layer = LAYER_DEFAULT;
/// Keyboard report as keycodes matrix
// Init as empty (just to be explicit)
static matrix_report report = (matrix_report){};

void
kb_init_internal()
{
	// Resets current layer
	current_layer = default_layer;
}

void
update_report()
{
	// This calls the slave and let him know that it will be polled
	// So in turn he will poll it's matrix and prepare to send the
	// result on the bus.

	// Data buffer to get from the i2c transfer
	// Adds one byte as it can change size and rounds by default
	kb_scan right;
	uint8_t read = twi_read_from(0xb, right.bytes, sizeof(right.bytes), true);
	if (read == sizeof(right.bytes)) {
		/* serial_print_str("Packet recieved: ["); DEBUG*/
		/* for (u8 i = 0; i < sizeof(right.bytes); ++i) { */
		/* 	serial_print_u8(right.bytes[i]); */
		/* 	if (i < sizeof(right.bytes) - 1) */
		/* 		serial_print_str(", "); */
		/* } */
		/* serial_print_str("]\n"); */
	} else {
		right = (kb_scan){}; // Inits as the read failed
		/* serial_print_str("Recieved "); */
		/* serial_print_u8(read); */
		/* serial_print_str(" bytes\n"); */
		if (twi_manage_timeout_flag(true)) {
		/* 	serial_print_str("Connection timeout\n"); */
			;
		}
	}

	// Scan matrix left
	kb_scan left = kb_scan_matrix();

	// Merge to 2 scans into the report
	for (u8 y = 0; y < KB_matrix_height; ++y) {
		for (u8 x = 0; x < KB_matrix_width / 2; ++x) {
			report.left[x][y] = kb_get_scan_bit(&left, x, y);
		}
		for (u8 x = 0; x < KB_matrix_width / 2; ++x) {
			report.right[x][y] = kb_get_scan_bit(&right, x, y);
		}
	}
	/* print_report(&report); DEBUG*/
}

static bool is_keycode(kb_keycode kc) {
	return kc > KEY_ERROR_UNDEFINED && kc < KEY_SAFE_RANGE;
}
static bool is_momentary(kb_keycode kc) {
	return kc >= MO(0) && kc <= MO(7);
}
static bool is_comentary(kb_keycode kc) {
	return kc >= CO(0) && kc <= CO(7);
}
static bool is_switch(kb_keycode kc) {
	return kc >= SW(0) && kc <= SW(7);
}
static bool is_definite(kb_keycode kc) {
	return kc >= DF(0) && kc <= DF(7);
}

void		kb_set_default_layer(u8 layer) {
	default_layer = layer;
	// TODO Write save default layer to EEPROM.
	// And write something to load it also
}

// Now it's the proper implementation.
// If you wish to make this better change the encoding for something smaller.

/// Scans the keyboard's matrix and returns the states
/// (They contains the keycodes)
matrix_report*
kb_get_report()
{
	// Copy old states so we can compare
	matrix_report old = report;
	// Gets the new report
	update_report();
	// Detect the state changes within the keys
	u8 state_change = 0;
	for (u8 i = 0; i < KB_matrix_width; ++i) {
		for (u8 j = 0; j < KB_matrix_height; j++) {
			// Detected a state change
			// Old states need to be normalized as they are keycodes
			// and not bools
			if (!!old.states[i][j] != !!report.states[i][j]) {
				// Remove only if the key was pressed
				if (old.states[i][j] != 0) {
					// Update the keyboard itself to not have the presses anymore
					kb_key_rm(old.states[i][j]);
				}
				// Store the change
				state_change = 1;
			}
		}
	}
	// _delay_ms(300);
	// There was no state change there is no need to go further
	if (state_change == 0) {
		// Keep the old values since they didn't change
		// and we have the keycodes already calculated.
		report = old;
		return &report;
	}

	// Reset to 0 to check for layer internal state change
	state_change = 0;
	// Scans the physical matrix
	// Handles internal layer states
	// TODO rewrite it as a switch case
	for (u8 i = 0; i < KB_matrix_width; ++i) {
		for (u8 j = 0; j < KB_matrix_height; j++) {
			// Gets the mapping

			uint16_t kc = layers[current_layer][j][i];
			// Then handles the internal states
			if (report.states[i][j]) {
				// Permits the user to interface with the detection layer
				kb_record_user(kc, &report);

				if (is_momentary(kc)) {
					// Switch to a layer and permit
					// to go back when released
					previous_layer = current_layer;
					current_layer = kc - KEY_MO_LAYER00;
					state_change = 1;
				} else if (is_comentary(kc)) {
					// Switch to a layer and permit
					// to go back when released
					current_layer = kc - KEY_CO_LAYER00;
					state_change = 1;
				} else if (is_switch(kc)) {
					// Switch to a layer
					current_layer = kc - KEY_SW_LAYER00;
					previous_layer = current_layer;
					state_change = 1;
				} else if (is_definite(kc)) {
					// Switch to a layer and sets it as default
					current_layer = kc - KEY_DF_LAYER00;
					previous_layer = current_layer;
					kb_set_default_layer(current_layer);
					state_change = 1;
				} else if (kc == KEY_SP) {
					// Patch to have momentary working
					state_change = 1;
				}

			}
		}
	}

	if (!state_change) {
		current_layer = previous_layer;
	}

	// Gets the keycodes from the report and the mapping
	for (u8 i = 0; i < KB_matrix_width; ++i) {
		for (u8 j = 0; j < KB_matrix_height; j++) {
			// Gets the mapping
			uint16_t kc = layers[current_layer][j][i];
			// If it's a keycode we'll add or remove it
			if (is_keycode(kc)) {
				// If the switch is pressed adds or removes
				if (report.states[i][j]) {
					// Record the keycode into the report
					report.states[i][j] = kc;
					kb_key_add(kc);
				}
			}
		}
	}
	return &report;
}
