#include "core.h"
#include "eeprom.h"
#include <avr/eeprom.h>

/// Saves a bit of data to the eeprom knowing it's size
/// Note: this asumes that the size of both pointer is the same
/// @param data a pointer to the data to save
/// @param eeprom pointer to the eeprom mapping
/// @param size size of the data to save
inline void
eeprom_save(
		void *data,
		void *eeprom,
		u8 size
) {
	eeprom_update_block(data, eeprom, size);
}

/// TODO; Apparently it's impossible to write to the eeprom at runtime,
/// look better into it as it seems bizzare.
/// Load a bit of data from the eeprom knowing it's size
/// Note: this asumes that the size of both pointer is the same
/// @param data a pointer to the data to load to
/// @param eeprom pointer to the eeprom mapping
/// @param size size of the data to load
inline void
eeprom_load(
		void *data,
		void *eeprom,
		u8 size
) {
	eeprom_read_block(data, eeprom, size);
}
