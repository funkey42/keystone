// Major include to get types and configuration
#include "core.h"

#include "encoder.h"
#include "sk9822.h"
#include "keyboard.h"
#include "serial.h"
#include "animations.h"
#include "twi.h"
#include "i41.h"
#include "time.h"
// Font data
#include "rre_8x12.h"

// This is to be removed it's so we don't read noinit memory
uint8_t dummy_port = 0;

const port_scanner	kb_cols[KB_matrix_width/2] = {
	[0] = (port_scanner){.port = &PINF, .pin = PINF0, .ddr = &DDRF},
	[1] = (port_scanner){.port = &PINF, .pin = PINF1, .ddr = &DDRF},
	[2] = (port_scanner){.port = &PINF, .pin = PINF4, .ddr = &DDRF},
	[3] = (port_scanner){.port = &PINF, .pin = PINF5, .ddr = &DDRF},
	[4] = (port_scanner){.port = &PINF, .pin = PINF6, .ddr = &DDRF},
	[5] = (port_scanner){.port = &PINF, .pin = PINF7, .ddr = &DDRF},
};
const port_scanner	kb_rows[KB_matrix_height] = {
	[0] = (port_scanner){.port = &PORTD, .pin = PORTD4, .ddr = &DDRD},
	[1] = (port_scanner){.port = &PORTD, .pin = PORTD5, .ddr = &DDRD},
	[2] = (port_scanner){.port = &PORTD, .pin = PORTD6, .ddr = &DDRD},
	[3] = (port_scanner){.port = &PORTD, .pin = PORTD7, .ddr = &DDRD},
};

enum {
	// Leds actions
	KEY_LED_ON = KEY_SAFE_RANGE,
	KEY_LED_OFF,
	KEY_LED_TOGGLE,
	KEY_ANIM_NEXT,
	KEY_AN = KEY_ANIM_NEXT,
	KEY_ANIM_PREV,
	KEY_AP = KEY_ANIM_PREV,
	// Principal color bindings
	KEY_LEDP_BRT_MORE,
	KEY_LEDP_BRT_LESS,
	KEY_LEDP_HUE_MORE,
	KEY_LEDP_HUE_LESS,
	KEY_LEDP_SAT_MORE,
	KEY_LEDP_SAT_LESS,
	// Accent color bindings
	KEY_LEDA_BRT_MORE,
	KEY_LEDA_BRT_LESS,
	KEY_LEDA_HUE_MORE,
	KEY_LEDA_HUE_LESS,
	KEY_LEDA_SAT_MORE,
	KEY_LEDA_SAT_LESS,
	// Menu actions
	KEY_MENU_UP,
	KEY_MENU_DOWN,
};

// This is the mapping of the keyboard.
// You can include special keys to handle some features of the keyboard
// like the menus and the leds for that look at the enum:
// `keyinternal` in "keyboard.h"
// for the rest it's made up with the pattern KEY_<key name>
// There are some short hands so you can look at the table with one screen.
// All the modifiers have a short hand for example.
// Also, the default layer is 0 but it can be changed while running.
// use `kb_set_default_layer(layer)` where layer is in [0-7].
// This enables yo to have persisting layer shift even with a deconnection.
const uint16_t layers[8][KB_matrix_height][KB_matrix_width] = {
	[0] = { // Default layer
		{KEY_TAB,    KEY_Q,    KEY_W,    KEY_E,    KEY_R, KEY_T,   /*|*/ KEY_Y,   KEY_U, KEY_I,     KEY_O,      KEY_P,         KEY_BACKSPACE},
		{KEY_ESC,    KEY_A,    KEY_S,    KEY_D,    KEY_F, KEY_G,   /*|*/ KEY_H,   KEY_J, KEY_K,     KEY_L,      KEY_SEMICOLON, KEY_QUOTE},
		{KEY_LSHIFT, KEY_Z,    KEY_X,    KEY_C,    KEY_V, KEY_B,   /*|*/ KEY_N,   KEY_M, KEY_COMMA, KEY_PERIOD, KEY_SLASH,     KEY_RETURN},
		{KEY_LCTRL,  KEY_MENU, KEY_LALT, KEY_LGUI, MO(1), KEY_SPC, /*|*/ KEY_SPC, MO(2), KEY_LEFT,  KEY_DOWN,   KEY_UP,        KEY_RIGHT},
	},
	[1] = { // Test layer
		{____, ____, ____, ____, ____,   ____, /*|*/ ____, ____,  ____, ____, ____, ____},
		{____, ____, ____, ____, ____,   ____, /*|*/ ____, ____,  ____, ____, ____, ____},
		{____, ____, ____, ____, ____,   ____, /*|*/ ____, ____,  ____, ____, ____, ____},
		{____, ____, ____, ____, KEY_SP, ____, /*|*/ ____, MO(3), ____, ____, ____, ____},
	},
	[2] = {
		{KEY_QUOTE, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, /*|*/ KEY_6, KEY_7,            KEY_8,            KEY_9,     KEY_0,     ____         },
		{____,      ____,  ____,  ____,  ____,  ____, /*|*/ ____,   KEY_LEFT_BRACE,   KEY_RIGHT_BRACE,  KEY_MINUS, KEY_EQUAL, KEY_BACKSLASH},
		{____,      ____,  ____,  ____,  ____,  ____, /*|*/ ____,   ____,             ____,             ____,      ____,      ____},
		{____,      ____,  ____,  ____,  MO(3), ____, /*|*/ ____,   KEY_SP,           ____,             ____,      ____,      ____},
	},
	[3] = {
		{____, KEY_AN,      KEY_AP, ____, ____, ____, /*|*/ ____, ____,  ____, ____, ____, ____},
		{____, KEY_LED_OFF, ____,   ____, ____, ____, /*|*/ ____, ____,  ____, ____, ____, ____},
		{____, ____,        ____,   ____, ____, ____, /*|*/ ____, ____,  ____, ____, ____, ____},
		{____, ____,        ____,   ____, ____, ____, /*|*/ ____, ____,   ____, ____, ____, ____},
	}
};

// static i16 total_rot = 0;
static animation anim;

const static RRE_Font *font = &rre_8x12;

static void *menu[];

static void *leds_menu[] = {
	"Primary Hue\0"
	"Primary Sat.\0"
	"Primary Value\0"
	"Secondary Hue\0"
	"Secondary Sat.\0"
	"Secondary Val.\0"
	"BACK\0",

	NULL, NULL,
	NULL, NULL,
	NULL, NULL,
	NULL, NULL,
	NULL, NULL,
	NULL, NULL,
	NULL, menu
};

static u16 backlight_slider_color = 0xffff;
static t_slider_data backlight_slider_data = {
	.value = &g_backlight,
	.max = 40,
	.color = &backlight_slider_color,
	.callback = i41_set_backlight,
};

/*
** Menu format:
** - String containing menu entries separated by '\0' (terminated w/ two '\0')
** - Entry 1 function pointer in `void (*)(void *)` or NULL if sub-menu
** - Entry 1 pointer on sub-menu or function metadata if not sub-menu
** - Entry 2 function pointer
** - Entry 2 data
** ...
*/

static void *menu[] = {
	"Screen Backlight\0"
	"Screen Sleep\0"
	"Terminal\0"
	"LEDS\0\0",

	i41_menu_slider_hook, &backlight_slider_data,
	i41_menu_sleep, NULL,
	i41_menu_terminal, NULL,
	NULL, leds_menu,
};

void setup() {
	kb_init();
	kb_init_matrix();
	serial_start();
	animate_init();
	encoder_init();
	// I2C start
	twi_init();
	twi_set_frequency(100000);
	twi_set_timeout_in_micros(16000, true);
	// Update animation from eeprom
	anim = animate_get_anim();
	i41_init();
	i41_init_menu(menu, font);
	i41_process_menu();
}

void kb_record_user(kb_keycode kc, matrix_report *report) {
	switch (kc) {
		case KEY_ANIM_NEXT:
			if (anim + 1 >= ANIM_max)
				anim = ANIM_none + 1;
			else
				anim += 1;
			animate_set_anim(anim);
			break;
		case KEY_ANIM_PREV:
			if (anim - 1 <= ANIM_none)
				anim = ANIM_max - 1;
			else
				anim -= 1;
			animate_set_anim(anim);
			break;
		case KEY_LED_OFF:
			anim = ANIM_none;
			animate_set_anim(anim);
			break;
	}
}

static u8 div = 0;

void loop() {
	// So we don't spam the bus
	if (div > 4) {
		// Begin matrix scan
		matrix_report *report = kb_get_report();

		animate_leds(report);
		div = 0;
	}
	div++;
	kb_send();
	i41_process_menu();
}

/// Init the interrupts for the encoder
void	encoder_interrupt_enable() {
	// This assumes that we are using PB4,5,7
	// So we need to mask those specific pins
	PCMSK0 |= (1 << PCINT4) | (1 << PCINT5) | (1 << PCINT7);
	// It's a PCINT[0-7] enable 
	PCICR |= (1 << PCIE0);
}

// Handle external interrupts here
ISR(PCINT0_vect) {
	// Screen is on SPI so it's here also

	// Update encoder
	encoder_update();
}
