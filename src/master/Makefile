TARGET		?= master
MCU			?= atmega32u4
PROGRAMMER	?= avr109
BAUD		?= 115200
F_CPU		?= 16000000 #16 Mhz quartz
CFLAGS		?= -mmcu=$(MCU) \
			   -DF_CPU=${F_CPU} \
			   -DARDUINO=10813 \
			   -DARDUINO_AVR_LEONARDO \
			   -DARDUINO_ARCH_AVR \
			   -DUSB_VID=0x2341 \
			   -DUSB_PID=0x8036 \
			   "-DUSB_MANUFACTURER=\"Funkey\"" \
			   "-DUSB_PRODUCT=\"Funkey D48SEBXS\"" \
			   $(foreach include,$(PATH_INC), -I$(include)) \
			   -std=gnu11 -fno-fat-lto-objects -Os -Wall -ffunction-sections -fdata-sections -flto -g \

CPPFLAGS	?= -mmcu=$(MCU) \
			   -DF_CPU=${F_CPU} \
			   -DARDUINO=10813 \
			   -DARDUINO_AVR_LEONARDO \
			   -DARDUINO_ARCH_AVR \
			   -DUSB_VID=0x2341 \
			   -DUSB_PID=0x8036 \
			   "-DUSB_MANUFACTURER=\"Funkey\"" \
			   "-DUSB_PRODUCT=\"Funkey D48SEBXS\"" \
			   $(foreach include,$(PATH_INC), -I$(include)) \
			   -fno-exceptions -fno-threadsafe-statics -fpermissive -std=gnu++11 -Os -Wall -ffunction-sections -fdata-sections -flto -g \

LDFLAGS		?= -Os -mmcu=$(MCU) -Wl,--gc-sections -flto -fuse-linker-plugin -g
CC			= avr-gcc
CCPP		= avr-g++

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
PORT 		?= /dev/ttyACM*
STTY 		?= stty -F
endif
ifeq ($(UNAME_S),Darwin)
PORT 		?= /dev/cu.usb*
STTY 		?= stty -f
endif

LIBS = \
	   ../../arduino \
	   ../../hid \

PATH_LIB := ../..
PATH_SRC := ..
PATH_INC := \
	../../include \
	./ \
	../../arduino/cores/arduino \
	../../arduino/variants/leonardo \
	../../arduino/libraries/EEPROM/src \
	../../arduino/libraries/SPI/src \
	../../arduino/libraries/SoftwareSerial/src \
	../../arduino/libraries/Wire/src \
	../../arduino/libraries/HID/src \
	../../hid/src \
	../../hid/src/HID-APIs \
	../../hid/src/KeyboardLayouts \
	../../hid/src/MultiReport \
	../../hid/src/port \
	../../hid/src/SingleReport \

PATH_BUILD := ../../.build/master
PATH_OBJ := $(PATH_BUILD)/obj
PATH_TARGET := $(PATH_BUILD)/target

SRCS = \
	$(PATH_SRC)/core/common/twi.c \
	$(PATH_SRC)/core/common/leds.c \
	$(PATH_SRC)/core/common/matrix.c \
	$(PATH_SRC)/core/common/wrapper.cpp \
	\
	$(PATH_SRC)/core/master/eeprom.c \
	$(PATH_SRC)/core/master/encoder.c \
	$(PATH_SRC)/core/master/i41.c \
	$(PATH_SRC)/core/master/led_animation.c \
	$(PATH_SRC)/core/master/matrix.c \
	$(PATH_SRC)/core/master/wrapper.cpp \
	\
	$(PATH_SRC)/master/main.c \

OBJS = $(addsuffix .o, $(basename $(SRCS:$(PATH_SRC)%=$(PATH_OBJ)%)))

all: $(TARGET).hex

include ../../makefiles/submake.mk
# Build a command for each lib to call them as library
$(foreach lib,$(LIBS), $(eval $(lib): FORCE;$(call cmd,$(lib),$(filter-out flash,$(MAKECMDGOALS)))))

include ../../makefiles/build.mk
include ../../makefiles/cc.mk

TO_CLEAN := $(PATH_BUILD)
TO_CLEAN_EXEC := rm -f $(TARGET).hex; $(foreach lib,$(LIBS),make -w -C $(lib) clean;)

$(PATH_TARGET)/$(TARGET).elf: $(LIBS) $(OBJS) | $$(@D)/.
	@printf $(COMPILING_FIRM)"\n"
	@$(CCPP) -o $@ $(LDFLAGS) $(OBJS) -Wl,--start-group $(foreach lib,$(LIBS), $(lib)/.build/target/$(notdir $(lib)).a) -lm -Wl,--end-group

FORCE:

%.hex: $(PATH_TARGET)/%.elf
	@printf $(MAKING_HEX)"\n"
	@avr-objcopy -O ihex -j .eeprom -j .text -j .data --set-section-flags=.eeprom=alloc,load --no-change-warnings --change-section-lma .eeprom=0 $< $@

build: $(TARGET).hex

flash: build
	@printf $(OPENCLOSE)"\n"
	@$(STTY) $(PORT) 1200
	@printf $(FLASHING_WAIT)
	@while :; do printf "."; sleep 0.5; [ -c $(PORT) ] && break; done
	@printf $(FLASHING)"\n"
	@avrdude -v -p $(MCU) -c $(PROGRAMMER) -b $(BAUD) -P $(PORT) -U flash:w:$(TARGET).hex

.PHONY: build flash clean FORCE
