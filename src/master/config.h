#pragma once

// Various encoder ports and pins
// Preferably you should have interrupts on them.
// And if you change them don't forget to change
// the `encoder_interrupt_enable` method.
#define ENC_port_plus		PINB
#define ENC_pin_plus		PINB4
#define ENC_port_minus		PINB
#define ENC_pin_minus		PINB5
#define ENC_port_push		PINB
#define ENC_pin_push		PINB7

// Various pins for the LEDs
#define LED_ddr_data		DDRD
#define LED_port_data		PORTD
#define LED_pin_data		PIND2
#define LED_ddr_clock		DDRD
#define LED_port_clock		PORTD
#define LED_pin_clock		PIND3

// Keyboard dimentions without the split
#define KB_matrix_height	4
#define KB_matrix_width		12
// Define the layout as us english (default)
#ifndef LAYOUT_US_ENGLISH
#define LAYOUT_US_ENGLISH
#endif

// I2C master config
#define TWI_matrix_address	0x0b
#define TWI_freq			1000000ul
#define TWI_timeout			100

#define SCREEN_WIDTH		240
#define SCREEN_HEIGHT		320
#define PORT_CS				PORTF
#define BIT_CS				PORTF5
#define PORT_RST			PORTE   // Same
#define BIT_RST				PORTE6  // Same
#define PORT_DC				PORTF
#define BIT_DC				PORTF7
#define PORT_BL				PORTB   // Same
#define BIT_BL				PORTB6  // Same
#define DEFAULT_BACKLIGHT   40 // From 0 to 40

// On PCB
#undef PORT_CS
#undef BIT_CS
#undef PORT_DC
#undef BIT_DC
#define PORT_CS				PORTB
#define BIT_CS				PORTB0
#define PORT_DC				PORTC
#define BIT_DC				PORTC7

// On PCB without Maxime's mod
#undef PORT_DC
#undef BIT_DC
#define PORT_DC				PORTE
#define BIT_DC				PORTE2

// Led config
#define ANIM_key_width			6
#define ANIM_key_height			4
#define ANIM_back_width			3
#define ANIM_back_height		2
